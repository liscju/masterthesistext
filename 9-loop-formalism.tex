
\chapter{Loop Formalism and loops analysis} \label{chap:loopformalism}

In this chapter loop formalism created by an author of this work is described. This formalism will help to define in a formal way
the loop transformations and algorithms of static analysis of the source code. Then it will be helpful for description of parallel code 
generation process.  

\section{Loop Definition}

The $FOR$ loop can be expressed as the set
$L$. $L$ is the set of 5-tuple with defined
order in the form:

\begin{align}
	\begin{split}
		L =  \{ i\_var, init\_value, end\_value, iter\_update(), E\}
	\end{split}
\end{align}

where:
\begin{enumerate}
\item $i\_var$ - iteration variable
\item $init\_value$ - initial value of iteration variable
\item $end\_value$ - end value of iteration variable
\item $iter\_update(i\_var)$ - is a function of iter\_var which defines updates of iteration variable in each iteration - $\Delta_{i\_var}$
\item $E = \{e_1, e_2,... \}$ is a set of statement expressions inside given loops
\end{enumerate}

For example for the following loops:

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}
for (i=1;i<=10;i++) {
   b = i*5;
   for (j=5;j<=15;j++) {
      a[b][j] = a[b-1][j];
   }
}
\end{lstlisting}
\end{minipage}

The two nested loops can be presented as an order set:

\begin{align}
	\begin{split}
		L = \{L_{i}, L_{j}\}
	\end{split}
\end{align}

where:

\begin{align}
	\begin{split}
		L_{i} = \{i, 1, 10, iter\_update(i), E_{i}\} \\
		iter\_update(i) = i+1\\
		E_{i} = \{b=i*5, a[b][j] = a[b-1][j]\} \\
		L_{j} = \{j, 5, 15, iter\_update(j), E_{j}\} \\
		iter\_update(j) = j+1\\
		E_{j} = \{a[b][j] = a[b-1][j]\} \\
	\end{split}
\end{align}

In the next sections it is shown how loop transformations
are expressed using this formalism.

\section{Expression definition}
The single expression in our algorithms we represent as a tuple. The first element is a left hand side value the second is a set of right hand side values:\\

\begin{align}
	\begin{split}
		E = \{LHS\_e, RHS\_e\} \\
	\end{split}
\end{align}
The function $rhs(E)$  returns $RHS\_e$, $lhs(E)$ returns $LHS\_e$.
$LHS_t(E) $ is a function which return left hand variable if it is an array or null if it isn't. $RHS_t(E) $ is a function which return right  hand variables  which are arrays. The function $subscript(var)$  is a method for extracting subscript expression from array variable. \\

\section{Dependency Analysis}
In this section the formalism for dependence analysis will be described. Following definitions describe write after write, read after write, write after read dependencies: 

\begin{align}
	\begin{split}
		RAW(E1, E2)  \equiv  \{ E1 \in predecessors(E2) \land  rhs(E2) \in lhs(E1) \} \\
		WAR(E1, E2)  \equiv  \{ E1 \in predecessors(E2) \land  rhs(E2) \in rhs(E1) \} \\
		WAW(E1, E2)  \equiv  \{ E1 \in predecessors(E2) \land  lhs(E2) \in lhs(E1) \} \\
	\end{split}
\end{align}

where:\\
predecessors(E2) - is the set of expressions that are before E2 epression in a source code

\section{Loop Invariants Code Motion}
Loop invariants code motion is a technique for finding loop invariants in a loop body and removing it outside the loop computations.
Loop Invariants code motion can be expressed using described formalism as the following Algorithm \ref{alg:loopInvariantCodeMotion}.
This algorithm will is used in developed system.

\begin{algorithm}
\caption{Loop Invariant Code Motion}
\label{alg:loopInvariantCodeMotion}
\begin{algorithmic}[1]
\Procedure{remove\_loop\_invariant}{$L\_nested\_set$}
\Comment{where $L\_nested\_set$ is the set of nested loops}
	\For{$l$ in $L\_nested\_set.reverse()$} 
	\For{$expr$ in $l.E$} \State $dependence \gets check\_if\_dependence\_from\_iter\_var(expr, l.i\_var) $
	\If {$dependence == false$}
	    \State $moveUp(l, l.next(), expr)$
	\EndIf
	\EndFor
	\EndFor
\EndProcedure
\end{algorithmic}
\end{algorithm}

The procedure of checking if variable depends on iteration variable is decribed in
Algorithm \ref{alg:checkIfDependenceFromIteration}. It uses $RAW$ procedure. 

\begin{algorithm}
\caption{Check If Dependence from Iteration}
\label{alg:checkIfDependenceFromIteration}
\begin{algorithmic}[1]
\Procedure{check\_if\_dependence\_from\_iter}{$expr, i\_var$} 
\Comment{Where $ expr$ is the expression and $ i\_var $ is the iteration variable}
	\If {$RAW(expr, i\_var) == true$}
	    \State \Return $true$
	    \Else
	    \State \Return $false$       
	\EndIf
\EndProcedure
\end{algorithmic}
\end{algorithm}

The next function needed for loop invariant code motion and loop peeling is removing expression from
loop and move it outer loop. This algorithm based on presented formalism is described in Algorithm \ref{alg:moveUpExpression}. 

\begin{algorithm}
\caption{Move Up Expression}
\label{alg:moveUpExpression}
\begin{algorithmic}[1]
\Procedure{move\_up\_expression}{$nested(L_1, L_2), expr$} \Comment{Where $ L_2 $ is directly nested loop inside $ L_1 $}
	\State $L_2.E \setminus expr$
	\State $L_1.E \cup expr$
\EndProcedure
\end{algorithmic}
\end{algorithm}


\section{Reduction Analysis}
Reduction analysis is a an process of finding of places in the source code where reduction operations are performed. Reduction operations are very common part in a lot of algorithms. As an example we can mention minimal and maximal value extraction from a given set, average value comuting etc. Reduction operation can be parallelized. Therefore openmp library offers $omp$ $reduction$ pragma. Reduction Analysis can be expressed as the following:
Algorithm \ref{alg:loopReductionAnalysis}:

\begin{algorithm}
\caption{Loop Reduction Analysis}
\label{alg:loopReductionAnalysis}
\begin{algorithmic}[1]
\Procedure{RecognizeReduction}{$L\_nested\_set$}
	\State {$ reductionVariables \leftarrow \emptyset$}
	\For{$l$ in $L\_nested\_set.reverse()$}
		\For{$ expr \leftarrow $ for all $l.E $}
			\If{$ expr $ in the form $ VAR = VAR$ $OP$ $.. $ and $ OP $ is one of \{+,-,*,/\}
			    and $ VAR $ is not used in other expression in $ IV $}
			    	\State{add $ VAR $ to set $ reductionVariables $}	
			\EndIf
		\EndFor
	\EndFor
	\State {return $ reductionVariables $}
\EndProcedure
\end{algorithmic}
\end{algorithm}


\section{Loop Transformations}
In this section formalism of loop transformations which were presented in previous section will be described.

\subsection{Loop Distribution}

\textbf{Input}: \\
Loop $ L =  \{ iter\_var, initial\_value, boundary\_value, iter\_update(), E1 \cup E2 \}$,  where
expressions in $E1$ has no dependencies on expressions in $E2$.\\
\\
\textbf{Output}: \\
Loops $ L_1 $ and $ L_2 $ with distributed instructions between them.

\begin{flalign}
	\begin{split}
		L =  \{ i\_var, init\_value, end\_value, iter\_update(), E1 \cup E2\} \\
		L_1 =  \{ i\_var, init\_value, end\_value, iter\_update(), E1\} \\
		L_2 =  \{ i\_var, init\_value, end\_value, iter\_update(), E2\} \\
	\end{split}
\end{flalign}

\subsection{Loop Fusion}

\textbf{Input}: \\
Loops $ L_{1} $ and $ L_{2} $.
\\\\
\textbf{Output}: \\
Loop $ L $ with expression being union of expression from input loops $ L_1 $ and $ L_2 $.

\begin{align}
	\begin{split}
		L_1 =  \{ i\_var, init\_value, end\_value, iter\_update(), E1\} \\
		L_2 =  \{ i\_var, init\_value, end\_value, iter\_update(), E2\} \\
		L =  \{ i\_var, init\_value, end\_value, iter\_update(), E1 \cup E2\} \\
	\end{split}
\end{align}

\subsection{Loop Unrolling}

\textbf{Input}: \\
Loop $ L $ and $ unroll\_ factor $ variable.  
\\\\
\textbf{Output}: \\
Loop $ L $ with unrolled instructions. 

\begin{flalign}
	\begin{split}
		L =  \{ i\_var, init\_value, end\_value, i\_var+\Delta_{i}, E\}  
	\end{split}
\end{flalign}

\begin{flalign}
	\begin{split}
		L =  \{ i\_var, init\_value, end\_value, i\_var+unroll\_factor\times \Delta_{i},unroll\_factor \times E\} 
	\end{split}
\end{flalign}

\subsection{Loop Tiling}

\textbf{Input}: \\
Loop $ L $ and tiling step $ B $.
\\\\
\textbf{Output}: \\
Loop $ L_1 $ that is tiled version of input loop $ L $.

\begin{align}
	\begin{split}
		L =  \{ i\_var, init\_value, end\_value, i\_var+\Delta_{i}, E\} 
	\end{split}
\end{align}

\begin{align}
	\begin{split}
		L_1 =  \{ i\_var_1, init\_value_1, end\_value_1, i\_var_1+B, E\} 
	\end{split}
\end{align}

\begin{align}
	\begin{split}
		L_2 =  \{ i\_var_2, init\_value=i\_var_1, end\_value=min(end\_value_1, i\_var_1+B),\\i\_var_2+\Delta_{i}, E\}
	\end{split}
\end{align}

where: $nested(L_1, L_2)$ it is nested relation, it means that $L_2$ loop is nested in $L_1$ outer loop.   

\subsection{If Instruction Removing}
The one of the method of optimization the efficiency of the code is trail of $if$ instruction removing.
In the system it was Algorithm \ref{alg:ifInstructionRemoving} created for such analysis. It analyzes condition expression of $if$ instruction and when its variables are known during compilation time it 
counts percentage of true conditions during program execution. If it is less than given treshold it removes $if$ instruction by moving iteration with true conditions outside analyzed loop. After that two loops were created both without $if$ instruction and with changed initial and end boundaries.   

\begin{algorithm}
\caption{If Instruction Removing}
\label{alg:ifInstructionRemoving}
\begin{algorithmic}[1]
%\for {$i = 0$ \to $i < 6$} 
%\state {$dsfd$} 
%\endfor
\For{$l$ in $L\_nested\_set.reverse()$} 
\For{$expr$ in $l.E$} 
\If {$type(expr) == IF\_Expression$}
        \If {$type(expr) == IF\_Expression$}
               \State $res \gets check\_condition(cond\_expr, L)$
               \If {$type(expr) == IF\_Expression$}
                      \State $number\_of\_true\_conditions \gets analyze\_condition(cond\_expr, L)$
                      \If {$number\_of\_true\_conditions \leq treshold$}
                            \State $iterations \gets remove\_iterations\_with\_true\_conditions()$
                            \State $remove\_if\_instruction()$
                            \State $put\_iterations\_with\_updated\_parameters(iterations)$
                      \EndIf
               \EndIf
        \EndIf
\EndIf
\EndFor
\EndFor
\end{algorithmic}
\end{algorithm}

Another problem in parallel programming are critical sections. In the system it was
Algorithm \ref{alg:criticalSectionExtraction} developed for these sections extraction. The first approach for extraction is
finding places where subscripts of LHS values depends on data (e.g. tab[tab[i]]) like in
histogram computation (section Chosen problems). The second one is to check if any LHS array
subscript expression is not a bijecton. If it is not a bijection the statement is potential
candidate for critical section, this fact can be very often proven be earlier described dependency
analysis algorithms.  

\begin{algorithm}[H]
\caption{Critical section extraction}
\label{alg:criticalSectionExtraction}
\begin{algorithmic}[1]
\For{$l$ in $L\_nested\_set$} 
\For {$e$ in $l.E$}
\If {common subscript($LHS_t(e))$ == true}
\State critical\_section $\vee$ synchronization
\Else
\State parallel\_section
\EndIf
\EndFor
\EndFor
\end{algorithmic}
\end{algorithm}

The last and very important Algorithm \ref{alg:memoryAnalysis} in the system is memory analysis.
Memory analysis is algorithm for computing size of private memory of iterations, size
of data of whole loop and shared memory between iterations. Algorithm in case of nested
loops run from most inner loop. If subscript expressions are bijections the memory size
estimations are quite easy (based on number of iterations of given loops) and checking if
expressions depends on iteration variable or doesn't or it is nested or current loop dependency.
In case of subscript expression are not bijections the computaton are more complex and in current
version of the system this functionality it isn't implemented.  

\begin{algorithm}[H]
\caption{Memory analysis}
\label{alg:memoryAnalysis}
\begin{algorithmic}[1]
\For{$l$ in $L\_nested\_set.reverse()$} 
\For{$expr$ in $l.E$} 
\For{$variable$ in $LHS_t(expr) \vee RHS_t(expr)$}
\If {$subscript(variable)$ is bijection $\wedge$ $dependent(set\_iter\_var(l))$} 
\If {$dependent(l.iter\_var) \wedge not\_dependent(nested\_iter\_var(l))$}
\State $memory\_private[l\_iter] \gets memory\_private[l\_iter] + 1$
\Else
\State $memory\_private[l\_iter] \gets memory\_private[l\_iter] + \prod_{i=0}^{|set\_nested\_iter\_var(l)|}number\_of\_iter_{i}$
\If{$not\_dependent(l.iter\_var)$}
\State $memory\_shared[l\_iter] \gets memory\_shared[l\_iter] + \prod_{i=0}^{|set\_nested\_iter\_var(l)|}number\_of\_iter_{i}$
\EndIf
\EndIf
\State $memory[l] \gets memory[l] + \prod_{i=0}^{|set\_iter\_var(l)|}number\_of\_iter_{i} $
\ElsIf{$subscript(variable)$ is bijection $\wedge$ $not\_dependent(set\_iter\_var(l))$}
\State $memory\_private[l\_iter] \gets memory\_private[l\_iter] + 1$
\State $memory[l] \gets memory[l] + 1$
\ElsIf{$subscript(variable)$ is not bijection $\wedge$ $not\_dependent(set\_iter\_var(l))$}
\State $memory\_private[l\_iter] \gets memory\_private[l\_iter] + 1$
\State $memory[l] \gets memory[l] + 1$
\ElsIf{$subscript(variable)$ is not bijection $\wedge$ $dependent(set\_iter\_var(l))$}
\If {$dependent(l.iter\_var) \wedge not\_dependent(nested\_iter\_var(l))$}
\State $memory\_private[l\_iter] \gets memory\_private[l\_iter] + 1$
\Else
\State $memory\_private[l\_iter] \gets memory\_private[l\_iter] +  set\_of\_values(subscript(variable))$
\If{$not\_dependent(l.iter\_var)$}
\State $memory\_shared[l\_iter] \gets memory\_shared[l\_iter] + set\_of\_values(subscript(variable))$
\EndIf
\EndIf
\State $memory[l] \gets memory[l] + set\_of\_values(subscript(variable)) $
\EndIf
\EndFor
\EndFor
\EndFor
\end{algorithmic}
\end{algorithm}

In Algorithm \ref{alg:memoryAnalysis} there are some helper functions. Theirs meaning are described below:\\
$set\_iter\_var(loop)$ - set of iteration variables consists of given loop iteration variable and all iteration variables of nested loops in given loop as a parameter\\
$nested\_iter\_var(loop)$ - all iteration variables of nested loops in given loop as a parameter\\
$not\_dependent(nested\_iter\_var(l))$ - true if dependency doesn't exist

\begin{mdframed}

In the Algorithm \ref{alg:matrixMultiplication} there is a matrix multiplication pseudocode presented.

\begin{algorithm}[H]
\caption{Matrix multiplication}
\label{alg:matrixMultiplication}
\begin{algorithmic}[1]
\For{$i$ in $1..100$} 
\For{$j$ in $1..100$}
        \State $tabC[i*100+j] \gets 0$
        \For {$k$ in $1..100$}
            \State $tabC[i*100+j] \gets tabC[i*100+j] + tabA[i*100+k] * tabB[k*100+j]$
       \EndFor
\EndFor
\EndFor
\end{algorithmic}
\end{algorithm}


Based on this code there is an execution of Algorithm \ref{alg:memoryAnalysis} of memory analysis described below:\\

first iteration: inside loop k\\
$memory\_private$: tabC - 1, tabA - 1, tabB - 1\\
$memory$: tabC - 1, tabA - 100, tabB - 100\\
$memory\_shared$: tabC - 1, tabB - 0, tabA  - 0\\

second iteration: loop j\\
$memory\_private$: tabC - 100, tabA - 100, tabB - 100\\
$memory$: tabC - 100, tabA - 100, tabB - 100*100\\
$memory\_shared$: tabC - 0, tabB - 0, tabA  - 100\\

third iteration: loop i\\
$memory\_private$: tabC - 100, tabA - 100, tabB - 100*100\\
$memory$: tabC - 100*100, tabA - 100*100, tabB - 100*100\\
$memory\_shared$: tabC - 0, tabB - 100*100, tabA  -  0\\

\end{mdframed}

The final Algorithm \ref{alg:mainAlgorithmOfTheSystem} presents steps of whole system and it consists of the 
algorithm described earlier in this section.

\begin{algorithm}
\caption{Main algorithm of the system}
\label{alg:mainAlgorithmOfTheSystem}
\begin{algorithmic}[1]
\State   Parsing the code by ANTLR
\State   Data Flow Graph building (with $WAW, RAW, WAR$ dependence in Section \ref{sec:GeneratingDataFlowGraph} and Abstract Syntax Tree in Section \ref{sec:abstractSyntaxTree}
\State   Dependence analysis in loops (pool of algorithms: banarjee, range test, Symbolic FME, barvinok counting, see Section \ref{subsec:dataDependenceInLoops})
\State   Vector dependence computing (see Section \ref{sec:calculatingDataDependencyVector})
\State   Loops invariant analysis (Algorithm \ref{alg:loopInvariantCodeMotion}) and reduction extraction (Algorithm \ref{alg:moveUpExpression})
\State   Loop transformations (interchanging, tiling, fusion, distribution), $if$ instructions inside loops analysis (Algorithm \ref{alg:ifInstructionRemoving}), critical section analysis (Algorithm \ref{alg:criticalSectionExtraction})
\State   Data analysis for each transformation from previous step (Algorithm \ref{alg:memoryAnalysis})
\State  Directives adding rules
\end{algorithmic}
\end{algorithm}

The Algorithm \ref{alg:directivesAddingRules} describes example insertion of openmp directives rules based
on analysis described in the work. In later sections real examples will be showed in which these rules will be used.  

\begin{algorithm}
\caption{Directives adding rules}
\label{alg:directivesAddingRules}
\begin{algorithmic}[1]
\State loops transformation - best configuration of loops is chosen based on data analysis (see matrix multiplication interchange and data locality in Section \ref{sec:matrixMultiplication})
\State parallel nested loops - omp parallel directive as high as possible (minimize openmp library overhead)
\State $if$ expression - loop distribution
\State reduction recognize - openmp reduction directive adding (see Algorithm \ref{alg:loopReductionAnalysis})
\State critical section extracted - locks creation (see histogram in Section \ref{sec:calculatingHistogram} and NBody in Section \ref{sec:NBodyProblem})
\State locks creation (see NBody in Section \ref{sec:NBodyProblem})
\State inside loops of different size of iterations - schedule directive (see NBody in Section \ref{sec:NBodyProblem})
\State data locality in parallel loops - omp simd and alligned directives (see matrix multiplication in Section \ref{sec:matrixMultiplication})  
\State private data and shared data - shared and private directives
\end{algorithmic}
\end{algorithm}
