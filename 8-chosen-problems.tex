\chapter{Chosen Problems} \label{chap:chosenProblems}

This chapter presents selected problems to be parallelized
to compare results between their execution sequentially
and when parallelized. There are different
parallelization strategies (even ones that fails in fastening
executions) presented. All tests were run on machine with Intel Xeon
Processor E5-2630 v3. It has following characteristics:

\begin{itemize}
	\item Number of cores - 8
	\item Number of threads - 16
	\item Processor Base Frequency - 2.40 GHz
	\item Cache - 20 MB
\end{itemize}

Additionally machine has 131 GB RAM memory. 

\section{N-Body Problem}
\label{sec:NBodyProblem}
The classical N-body problem simulates the evolution
of a system of N bodies, where the force exerted
on each body arises due to its interaction with all
the other bodies in the system. N-body algorithms
have numerous applications in areas such as
astrophysics, molecular dynamics and plasma physics.
The simulations proceeds over timestamps, each time
computing the net force on every body and thereby updating
its position and other attributes. If all pairwise forces
are computed directly this requires $ O(N^{2})$
operations at each timestep. Pseudocode for progressing
the simulation one time step looks like:

\begin{algorithm}
\caption{N-Body one time step simulation}
\begin{algorithmic}[1]
\Procedure{SimulateOneStep}{}
	\For{each body q}
		\State {Initialize net force on q to zero}
		\For{every body k besides q}
			\State {Calculate the force k exerts on q}
			\State {Add this force to the net force on q}
		\EndFor
	\EndFor
	\For{each body q}
		\State {Update q position and velocity}
	\EndFor
\EndProcedure
\end{algorithmic}
\end{algorithm}

\subsection{Code}
We tested four different approaches to parallelize NBody.
All codes use two macros:

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}
/* Store in force_ij[] the force on body i by body j */
#define Calc_Force_ij() 

/* Update velocity and position of body i */
#define Step_Body_i()   
\end{lstlisting}
\end{minipage}

In first approach we calculate for each body 
forces between this body and each other and updates
body position and velocity in parallel:

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}
for (k=0; k<num_steps; k++) {
    #pragma omp parallel for private(j, PRIVATE_VARS)
    for (i=0; i<num_bodies; i++) {
        tot_force_i[X] = 0.0;
        tot_force_i[Y] = 0.0;

        /* Compute total force f(i) on each body i */
        for (j=0; j<num_bodies; j++) {
	    if (j==i) continue;

	    Calc_Force_ij();

	    tot_force_i[X] += force_ij[X];
	    tot_force_i[Y] += force_ij[Y];
        }

        Step_Body_i();
    }
}
\end{lstlisting}
\end{minipage}

Detecting possibility of the inserting inserting omp parallel pragma
is done in the system, see Algorithm \ref{alg:directivesAddingRules}.

In second approach we calculate forces between all bodies
in parallel, then we calculate total force on each body and
updates position and velocity in parallel:

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}
for (k=0; k<num_steps; k++) {
    #define forces(i,j,x) \
      forces_matrix[x + 2*(j + num_bodies*i)]

    /* Fill in a big nxn table of forces */
    #pragma omp parallel for private(j, PRIVATE_VARS)
    for (i=0; i<num_bodies; i++) {
        for (j=i+1; j<num_bodies; j++) {
            Calc_Force_ij();

            forces(i, j, X) = force_ij[X];
            forces(i, j, Y) = force_ij[Y];
        }
    }

    /* Compute total force f(i) on each body i */
    #pragma omp parallel for private(j, PRIVATE_VARS)
    for (i=0; i<num_bodies; i++) {
        tot_force_i[X] = 0.0;
        tot_force_i[Y] = 0.0;

        for (j=0; j<i; j++) {
            tot_force_i[X] -= forces(j, i, X);
            tot_force_i[Y] -= forces(j, i, Y);
        }
        for (j=i+1; j<num_bodies; j++) {
            tot_force_i[X] += forces(i, j, X);
            tot_force_i[Y] += forces(i, j, Y);
        }

        /* Update velocity and position of each body,
           by numerical integration */
        Step_Body_i();
    }    
}
\end{lstlisting}
\end{minipage}

The third approach is to calculate total force for all
bodies in parallel(using critical section to enforce not updating
same variables by two threads), then do update body's position and
velocity in parallel. Critical section extraction is done in
system while parallelizing loop, see Algorithm \ref{alg:directivesAddingRules}.

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}
for (k=0; k<num_steps; k++) {
    #define forces(i,x) forces_matrix[x + 2*i]

    #pragma omp parallel for private(j, PRIVATE_VARS)
    for (i=0; i<num_bodies; i++) {
        for (j=i+1; j<num_bodies; j++) {
            Calc_Force_ij();

            #pragma omp critical
            {
                forces(i,X) += force_ij[X];
                forces(i,Y) += force_ij[Y];
                forces(j,X) -= force_ij[X];
                forces(j,Y) -= force_ij[Y];
            }
        }
    }
}
\end{lstlisting}
\end{minipage}

The forth approach is similiar to third, but we use lock to
synchronize calculations of total forces. Locks are inserted in system
after calculating critical section of the application.

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}
for (k=0; k<num_steps; k++) {
    #pragma omp parallel for private(j, PRIVATE_VARS),\
                             schedule(static, 1)
    for (i=0; i<num_bodies; i++) {
        for (j=i+1; j<num_bodies; j++) {
            Calc_Force_ij();

            omp_set_lock(&lock[i]);
            forces(i,X) += force_ij[X];
            forces(i,Y) += force_ij[Y];
            omp_unset_lock(&lock[i]);

            omp_set_lock(&lock[j]);
            forces(j,X) -= force_ij[X];
            forces(j,Y) -= force_ij[Y];
            omp_unset_lock(&lock[j]);
        }
    }

    #pragma omp parallel for private(j, PRIVATE_VARS)
    for (i=0; i<num_bodies; i++) {
        Step_Body_i();
    }  
}
\end{lstlisting}
\end{minipage}

\section{Matrix multiplication}
\label{sec:matrixMultiplication}
Matrix multiplication is used thoroughly in science. It is used
in solving a linear system of equations that can be modeled
as:

\begin{align}
	\begin{split}
		Ax = b
	\end{split}
\end{align}

wheer x contains the unknowns, A is the coefficients of the equation
and b contains the values of the right side of equations.
\\
If $ A $ is an $ n x m $ matrix and $ B $ is an $ m x p $ matrix,
the matrix product $ AB $ is defined to be $ n x p $ matrix, each
element of the form:

\begin{align}
	\begin{split}
		(AB)_{ij} = \sum_{k=1}^{m}A_{ik}B_{kj}
	\end{split}
\end{align}

Matrix multiplication can be done by the following piece of code:

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}
int *matrix_multiply(int *matrix1,
                     int *matrix2,
                     int size) {

  int i;
  int *matrix_result = initialize_matrix(size);
  matrix_fill_zeros(matrix_result, size);

  for (i=0;i < size;i++) {
    int j,k;
    for (j=0;j < size;j++) {
      for (k=0;k < size;k++) {
        matrix_result[matrix_get_index(size, i, j)] += 
          matrix1[matrix_get_index(size, i, k)] *
          matrix2[matrix_get_index(size, k, j)];
      }
    }
  }
  return matrix_result;
}
\end{lstlisting}
\end{minipage}

\subsection{Parallelizing}
There are several different methods used for parallelizing
matrix multiplication problem. Few of them are presented
in \cite{ParallelMethodsForMatrixMultiplication}. Data in
matrix can be decomposed in distinct matter, for example:

\begin{itemize}
	\item each subtask hold one row of matrix
	      $ A $ and one column from matrix $ B $
	      at a time
	\item each subtask hold one row of matrix
	      $ A $ and one row from matrix $ B $
	\item matrixes $ A $ and $ B $ are divided
	      into blocks, each subtask calculates
	      corresponding block value
\end{itemize}

However in this example we would like to show how exploiting SIMD vectorization
can lead to better program executions. To make long story short, a vector
is an operand of the instruction that contains a set of data elements that
were packed into an array. SIMD(Single Instruction Multiple Data) in this example
means that the few operations that needs to be performed on multiple data elements,
can be performed at once packed in the vector.

Paper \cite{TutorialRealWorldExamplesOfVectorization} describes how matrix multiplication
can be implemented to support SIMD vectorization. The base parallel version of matrix
multiplication that were chosen was the following(this kind of possible parallelism
is detected in the system, see Algorithm \ref{alg:directivesAddingRules}):

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}
int *matrix_multiply_parallel_base(int *matrix1,
 				   int *matrix2,
 				   int size) {

  int i;
  int *matrix_result = initialize_matrix(size);
  matrix_fill_zeros(matrix_result, size);

#pragma omp parallel for
  for (i=0;i < size;i++) {
    int j,k;
    for (j=0;j < size;j++) {
      for (k=0;k < size;k++) {
        matrix_result[matrix_get_index(size, i, j)] += 
          matrix1[matrix_get_index(size, i, k)] *
          matrix2[matrix_get_index(size, k, j)];
      }
    }
  }
  return matrix_result;
}
\end{lstlisting}
\end{minipage}

OpenMP supports SIMD processing by the simd pragma, the easiest approach to
make this code supports SIMD vectorization would be to to add this pragma
to the most inner loop. In our system SIMD vectorization is one of the steps
done after doing code analysis, see Algorithm \ref{alg:directivesAddingRules}.

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}
int *matrix_multiply_parallel_bad_vectorization(int *matrix1, 
                                                int *matrix2,
                                                int size) {

  int i;
  int *matrix_result = initialize_matrix(size);
  matrix_fill_zeros(matrix_result, size);

#pragma omp parallel for
  for (i=0;i < size;i++) {
    int j,k;
    for (j=0;j < size;j++) {
#pragma omp simd
      for (k=0;k < size;k++) {
        matrix_result[matrix_get_index(size, i, j)] += 
          matrix1[matrix_get_index(size, i, k)] *
          matrix2[matrix_get_index(size, k, j)];
      }
    }
  }
  return matrix_result;
}
\end{lstlisting}
\end{minipage}

Unfortunately this version is not taking into consideration how data is located
in the memory. In the following figure it is presented how data needed to calculate
single cell of the result matrix is scattered, it can be observed that part of data
needed to calculate single cell of the result matrix is a non-unit stride.

\begin{figure}[H]
	\centering
	\includegraphics[scale=1]{./Diagrams/baseParallel.eps}
	\caption{Data accessed in matrixes in bad vectorization}
\end{figure}

To make it work better we can interchange inner loops, this make
i rows of the result matrix updated every k iteration and the most
inner loops can be done in vector. Interchanging loops are one of
the possible transformations implemented in our system, they are done
before inserting pragmas according to analysed data.

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}
int *matrix_multiply_parallel_right_vectorization(int *matrix1,
                                                  int *matrix2,
                                                  int size) {

  int i;
  int *matrix_result = initialize_matrix(size);
  matrix_fill_zeros(matrix_result, size);

#pragma omp parallel for
  for (i=0;i < size;i++) {
    int j,k;
    for (k=0;k < size;k++) {
#pragma omp simd
      for (j=0;j < size;j++) {
        matrix_result[matrix_get_index(size, i, j)] += 
        matrix1[matrix_get_index(size, i, k)] *
        matrix2[matrix_get_index(size, k, j)];
      }
    }
  }
  return matrix_result;
}
\end{lstlisting}
\end{minipage}

The described behaviour is shown in the following diagram:

\begin{figure}[H]
	\centering
	\includegraphics[scale=1]{./Diagrams/rightSimdVectorization.eps}
	\caption{Data accessed in matrixes in right vectorization}
\end{figure}

We can make it run even better we can do the manual blocking. This is
to divide matrix into blocks, iterate over them and traverse block contents
calculating result. Block tiling is another example of transformation included
in the system, the size of the block is dependent of the target processor cache
size. The code is the following:

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}[breaklines=true]
int *matrix_multiply_manual_blocking(int *matrix1,
                                     int *matrix2,
                                     int size) {
  const int block_size = 64;

  int k1,j1;
  int *matrix_result = initialize_matrix(size);
  matrix_fill_zeros(matrix_result, size);

  for (k1 = 0; k1 < size; k1 += block_size) { 
#pragma omp parallel for private(j1) shared(k1)
    for (j1 = 0; j1 < size; j1 += block_size) {
      int i,k,j;
      for (i=0;i < size;i++) {
        for (k=k1;k < minimum(k1 + block_size, size);k++) {
#pragma omp simd
          for (j=j1;j < minimum(j1 + block_size, size);j++) {
            matrix_result[matrix_get_index(size, i, j)] += 
              matrix1[matrix_get_index(size, i, k)] *
              matrix2[matrix_get_index(size, k, j)];
          }
        }
      }
    }
  }
  return matrix_result;
}
\end{lstlisting}
\end{minipage}

\begin{figure}[H]
	\centering
	\includegraphics[scale=1]{./Diagrams/manualBlocking.eps}
	\caption{Data accessed in matrixes in manual blocking}
\end{figure}

In \cite{TutorialRealWorldExamplesOfVectorization} there are
presented performance results of these transformation.

\section{Counting average on big set of data}
It is common in today's world to gather statistics
on big sets of data. Databases and data warehouses
are used to store and retrieve information counted
in many gigabytes. It is essential for this system
to be able to compute analytical reports as fast
as possible. One of the simplest example is to
calculate an average value from large sets of 
samples. An example how the average can be calculated is shown
in the following code snippet:

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}
float calculate_average(int *array, int array_len) {
	int i;
	long long int sum = 0;

	for (i=0;i < array_len;i++) {
		sum += array[i];
	}

	float average = sum / (float)array_len;

	return average;
}
\end{lstlisting}
\end{minipage}

\subsection{Parallelizing}
Gathering statistics on data like averages are in
general very easy to parallelize. This kind of
problems are sometimes called embarassingly parallel
in literature to emphasize that little or no effort
is needed to separate the problem into parallel tasks.
In this particular example reduction clause in OpenMP
is perfect fit to do this, as shown in the following
example:

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}
float calculate_average_parallel(int *array, int array_len) {
	int i;
	long long int sum = 0;

#pragma omp parallel for reduction (+:sum)
	for (i=0;i < array_len;i++) {
		sum += array[i];
	}

	float average = sum / (float)array_len;

	return average;
}
\end{lstlisting}
\end{minipage}

Extracting reduction is one of the most important steps in our
system(see Algorithm \ref{alg:mainAlgorithmOfTheSystem}), because reduction
is very efficient and elegant way to parallelize loops.

\subsection{Results}
In the following chart there is presented difference of
average time of executions between sequential version
and parallelized using reduction with 16 threads:

\begin{figure}[H]
	\centering
	\includegraphics[scale=1]{./Diagrams/ComparisonDifferentAverageExecutionTime.eps}
	\caption{Comparison of executions between sequential and parallel version}
\end{figure}

The full data sets for the sequential execution is the following(average time
is calculated from four invocations for the same number of threads and
count of points):

\begin{center}
\begin{longtable}{c|c|c}
\caption{Full results for sequential} \\
\hline \hline
\textbf{Threads} &
\textbf{Count of Points} & 
\textbf{Average Time(ms)} \\ 
\hline
1 & 50000000 & 1425,5\\
1 & 100000000 & 2857,25\\
1 & 200000000 & 5713\\
\hline
\end{longtable}
\end{center}

The full data sets for parallel execution using reduction is the following
(average is once again calculated from four invocations for given number of
threads and count of points):

\begin{center}
\begin{longtable}{c|c|c}
\caption{Full results for parallel} \\
\hline \hline
\textbf{Threads} &
\textbf{Count of Points} & 
\textbf{Average Time(ms)} \\ 
\hline
1 & 50000000 & 1216\\
8 & 50000000 & 288\\
16 & 50000000 & 180,25\\

1 & 100000000 & 2429,75\\
8 & 100000000 & 562\\
16 & 100000000 & 346\\

1 &  200000000 & 4857\\
8 &  200000000 & 993,75\\
16 & 200000000 & 655,50\\
\hline
\end{longtable}
\end{center}

\section{Calculating histogram}
\label{sec:calculatingHistogram}
Calculating average is shown in the previous section as an example of gathering statistics
on big sets of data. Another
thing that is worth calculating is histogram. Histogram is a
graphical representation of the distribution of numerical data.
Its construction is very simple. In the first step
we decide how to divide input data into consecutive, non-overlapping
intervals of variable. In the next step we calculate how many items 
in input data belongs to given interval. Histogram can be calculated
sequentially by the following code:

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}
for (i=0;i < points_len;i++) {
  int curr_point_interval_index = 
  calculate_point_interval_index(
  points[i], min, max, interval_count);

  intervals[curr_point_interval_index]++;
}
\end{lstlisting}
\end{minipage}

\subsection{Parallelizing}
The easiest way to parallelize calculating histogram is to divide
input data value between threads of execution, calculate to which
interval each input belongs and put this item into appropriate
interval. However, to make operation of putting items into the
interval thread safe, we have to make sure that only one of threads
is doing it at a time. To do this, we make putting item done
in critical section of the loop(extracting critical section is one of
the responsibilities of our system). To make calculation run more
efficiently instead of locking in critical section on the all intervals,
we lock only on the index on which the item will be put. The code doing
this is following:

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}
#pragma omp parallel for private(i)
for (i=0;i < points_len;i++) {
  int curr_point_interval_index = 
  calculate_point_interval_index(
  points[i], min, max, interval_count);

  omp_set_lock(&indexlocks[curr_point_interval_index]);

  intervals[curr_point_interval_index]++;

  omp_unset_lock(&indexlocks[curr_point_interval_index]);
}
\end{lstlisting}
\end{minipage}

Unfortunately, the critical section makes calculation run in nearly sequential time
because each thread of execution is waiting when another thread put item in the same
index. To make it run more efficiently we can make each thread calculates histogram
in for its subset of the data and later reduce(see reduction phase in \ref{alg:directivesAddingRules})
result from all threads. It can be done with the following code:

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}
#pragma omp parallel
{
  int i, j, *intervals_private;  
  intervals_private = calloc(interval_count, sizeof(int));

  #pragma omp for nowait    
  for (i=0;i < points_len;i++) {
    int curr_point_interval_index = 
    calculate_point_interval_index(
    points[i], min, max, interval_count);

    intervals_private[curr_point_interval_index]++;
  }

  #pragma omp critical
  {
    for (i=0;i < interval_count;i++) {
      intervals[i] += intervals_private[i];
    }
  }
}

return intervals;
}
\end{lstlisting}
\end{minipage}

\subsection{Results}
Test were run for the three presented variation of the implementation:

\begin{itemize}
	\item Sequential
	\item Simple Parallel with critical section
	\item Parallelizing using reduction
\end{itemize}

The results are the following(average time
is calculated from four invocations of an algorithm for the same
number of threads and data):

\begin{table}[h]
\caption{Results for sequential invocation}
\centering
\begin{tabular}{c c c c c c}
\hline\hline
Threads & Count Of Points & Min & Max & Count of Intervals & Average Time(ms) \\ [0.5ex]
\hline
1 & 50000000 & 0 & 50000 & 300 & 681 \\
1 & 100000000 & 0 & 50000 & 300 & 1358,25 \\
1 & 200000000 & 0 & 50000 & 300 & 2491 \\
\hline
\end{tabular}
\label{table:nonlin}
\end{table}

\begin{table}[h]
\caption{Parallelizing with critical section}
\centering
\begin{tabular}{c c c c c c}
\hline\hline
Threads & Count Of Points & Min & Max & Count of Intervals & Average Time(ms) \\ [0.5ex]
\hline
32 & 50000000 & 0 & 50000 & 300 & 1605,8 \\
32 & 100000000 & 0 & 50000 & 300 & 3117 \\
32 & 200000000 & 0 & 50000 & 300 & 6256,2 \\
\hline
\end{tabular}
\label{table:nonlin}
\end{table}

\begin{table}[h]
\caption{Parallelizing using reduction}
\centering
\begin{tabular}{c c c c c c}
\hline\hline
Threads & Count Of Points & Min & Max & Count of Intervals & Average Time(ms) \\ [0.5ex]
\hline
32 & 50000000 & 0 & 50000 & 300 & 87 \\
32 & 100000000 & 0 & 50000 & 300 & 140,6 \\
32 & 200000000 & 0 & 50000 & 300 & 242 \\
\hline
\end{tabular}
\label{table:nonlin}
\end{table}

As can be seen in the data naive parallelizing using critical 
section is making solution perform even worse than sequential.
This is because there is a little computation done in parallel
as calculating index is a matter of few additions and 
multiplications and the cost of locking and unlocking is too high
in this problem. Parallelizing using reduction gives great
speedup. In addition, the advantage of using
it rises with the size of the problem. The comparison between these two implementation can
be seen in the following chart:

\begin{figure}[H]
	\centering
	\includegraphics[scale=1]{./Diagrams/HistogramComparisonOfParallelizationTechniques.eps}
	\caption{Comparison of time of execution of different histogram calculation}
\end{figure}

The data gathered for Naive Parallel using critical section for different
number of threads is the following(average time is calculated from four
invocation for the same number of threads and input data):

\begin{center}
\begin{longtable}{c|c|c|c|c|c}
\caption{Full results for naive parallelization} \\
\hline \hline
\textbf{Threads} &
\textbf{Count of Points} & 
\textbf{Min} & 
\textbf{Max} & 
\textbf{Count of Intervals} & 
\textbf{Average Time(ms)} \\ 
\hline
\endhead
1 & 50000000 & 0 & 50000 & 300 & 1150,40\\
8 & 50000000 & 0 & 50000 & 300 & 2723,60\\
16 & 50000000 & 0 & 50000 & 300 & 2074,80\\
32 & 50000000 & 0 & 50000 & 300 & 1605,80\\

1 & 100000000 & 0 & 50000 & 300 & 2327,20\\
8 & 100000000 & 0 & 50000 & 300 & 5338,80\\
16 & 100000000 & 0 & 50000 & 300 & 4281,40\\
32 & 100000000 & 0 & 50000 & 300 & 3117\\

1 &  200000000 & 0 & 50000 & 300 & 4662,80\\
8 &  200000000 & 0 & 50000 & 300 & 10589,20\\
16 & 200000000 & 0 & 50000 & 300 & 8408,80\\
32 & 200000000 & 0 & 50000 & 300 & 6256,20\\
\hline
\end{longtable}
\end{center}

The way how different number of threads influence the execution time
for three different sizes of the problem is presented below:

\begin{figure}[H]
	\centering
	\includegraphics[scale=1]{./Diagrams/HistogramNaiveParallelizationComparision.eps}
	\caption{Comparison of time of execution of different sizes using naive parallelization}
\end{figure}


The data for parallelization using reduction for different number
of threads look like the following(average time is calculated from
four invocation for the same number of threads and input data):
\\
\\
\\
\begin{center}
\begin{longtable}{c|c|c|c|c|c}
\caption{Full results for parallelization using reduction} \\
\hline \hline
\textbf{Threads} &
\textbf{Count of Points} & 
\textbf{Min} & 
\textbf{Max} & 
\textbf{Count of Intervals} & 
\textbf{Average Time(ms)} \\ 
\hline
\endhead
1 & 50000000 & 0 & 50000 & 300 & 627\\
8 & 50000000 & 0 & 50000 & 300 & 154,40\\
16 & 50000000 & 0 & 50000 & 300 & 84,60\\
32 & 50000000 & 0 & 50000 & 300 & 87\\

1 & 100000000 & 0 & 50000 & 300 & 1245,6\\
8 & 100000000 & 0 & 50000 & 300 & 261,80\\
16 & 100000000 & 0 & 50000 & 300 & 155\\
32 & 100000000 & 0 & 50000 & 300 & 140,60\\

1 &  200000000 & 0 & 50000 & 300 & 3119,8\\
8 &  200000000 & 0 & 50000 & 300 & 458,80\\
16 & 200000000 & 0 & 50000 & 300 & 252,80\\
32 & 200000000 & 0 & 50000 & 300 & 242\\
\hline
\end{longtable}
\end{center}
