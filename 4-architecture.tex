\chapter{Architecture} \label{chap:architecture}

This chapter provides a high level description of the design
and implementation of JaPar - tool for pararelizing application
written in C language. JaPar is command line application that
takes path to C source file in argument and output corresponding
C source with added OpenMP directives.

\section{Introduction to JaPar}

The JaPar is the static code analysis tool that responsibility is 
inserting directives to the source code when some conditions are
met. It starts with doing lexical analysis of the source code.
In this phase it breaks the input into individual words called tokens.
Lexical analysis is also responsible for cutting out whitespaces
and comments. The Lexical analyzer takes a stream of characters
and produces a stream of names, keywords and punctuation marks. In
the next phase the tokens are parsed to form expressions. It is
usually described by context-free grammars. The output product of
the parsing phase is abstract syntax tree. It contains all information
from parsing tree in convenient form. The lexical analysis and 
parsing is done in the project with ANTLR tool. After this the code
is analyzed using data dependency testing algorithms to check if given
loop can be paralellized. For now those algorithms are Fourier Motzkin
Algorithm and Banerjee algorithm. This information is used in doing
transformation on the tree for example when instruction in the loop
are independent the loop can be unwind to several and done concurrently.

\section{Architecture} \label{sec:Architecture}
Project consists of the three separate parts. The first one is parsing
frontend. It is reponsible for converting the source code into the
appropriate tree that is recognized by the system. The second
part is the core of the system. It is responsible for doing static
code analysis, gathering information about data dependencies and
obtaining information which loops can be parallelyzed. The last part
is the backend which is reponsible for doing loop transformations on
the tree given information about data dependencies and
information how loops can be paralelized. The general architecture of
the solution can be described by the following diagram:

\begin{figure}[H]
	\centering
	\includegraphics[scale=1.5]{./Diagrams/GeneralArchitectureOfSolution.eps}
	\caption{High level architecture of a project}
\end{figure}

The most important property of this design is frontend and backend
modules can be changed. Changing frontend enables supporting
new languages, for example Java Bytecode Parser can translate
Java Bytecode to the tree that system recognizes. Changing backend
enables doing different types of pararelization. For example there
can be two backends for the C language: Pthreads backend and OpenMP
backend. Pthreads backend could be responsible for doing transformation
of loops to enable pararellism by using Pthreads libraries, whereas
OpenMP backend would enable pararellism by using OpenMP directives.
When the frontend and backend is set the data transfer in the system 
looks like in the following image:

\begin{figure}[H]
	\centering
	\includegraphics[scale=1.0]{./Diagrams/ArchitectureOfProject.eps}
	\caption{Data transfer in the project}
\end{figure}

Analysing the source code can be divided into three phases:

\begin{enumerate}
	\item Calculating input/output dependencies of nodes
	\item Generating Data Flow Graph
	\item Analysing Loops
\end{enumerate}

This is presented in the diagram:

\begin{figure}[H]
	\centering
	\includegraphics[scale=1.0]{./Diagrams/AnalysingTheSourceCode.eps}
	\caption{Analysing the source code}
\end{figure}

Analysing Loops phase is responsible for:

\begin{enumerate}
	\item calculating dependencies in loops
	\item calculating dependency vectors in loops
	\item calculating memory usages in loops
	\item static code analysis to detect critical sections
\end{enumerate}

This is presented in the following diagram:

\begin{figure}[H]
	\centering
	\includegraphics[scale=1.0]{./Diagrams/AnalysingLoops.eps}
	\caption{Analysing loops}
\end{figure}

Calculating dependencies in loops phase is running dependency
tests on loops to determine if there loops are carrying
dependencies. Calculating dependency vector phase is responsible
for calculating distance vectors for found dependencies. Additionally
there are calculated statistics of memory usages in loops. 
Other static code analysis can determine additional information in
code such as if there are parts of source code that should be
put in critical section in case of parallelizing them.

After analysis there is a transformation phase. First of all it
needs to make a decision how to transform the source code according
to data from analysis phase. After this the appropriate transformations
are applied and finally the output source code is generated, this following
diagram is showing this:

\begin{figure}[H]
	\centering
	\includegraphics[scale=1.0]{./Diagrams/TransformingLoops.eps}
	\caption{Transforming loops}
\end{figure}

This phase is dependent of programming languages that is analyzed and
the library used to parallelized application. For example there are different
techniques that are used when C code is parallelized using OpenMP library
and Pthreads.

\section{Tools} \label{sec:Tools}
Project is written with Java using Maven as build system. Parsing is done
with ANTLR that is integrated with Maven. Every time the project compiles
ANTLR generates lexer and parser. Project is tested using JUnit. More
information on parsing can be found in \cite{ModernCompilerImplementationInJava},
about ANTLR in \cite{TheDefinitiveANTLR4Reference}

\section{Lexer and Parser} \label{sec:LexerAndParser}
Lexer and Parser are created automatically by ANTLR from file containing
grammar of the C programming language. Lexer is responsible for taking
file as input and generate stream of tokens. Parser is responsible for
transforming stream of tokens into syntax tree. Generated syntax tree
are objects that are mapped by ANTLR from production in grammar
into objects. For example the following production:

\begin{grammar}
<multiplicativeExpression> ::= <multiplicativeExpression> \% <castExpression> 
                            \alt ...
\end{grammar}


described as multiplicativeExpressionDivRest in grammar file, 
generates associate object of the class 
C11Parser.MultiplicativeExpressionDivRestContext
that contains two getters: 

\begin{enumerate}
	\item multiplicativeExpression() - contains syntax tree for multiplicativeExpression
	\item castExpression() - contains syntax tree for castExpression
\end{enumerate}

\section{Generating abstract syntax tree}
Syntax Tree generated from parser contains all information about input
source code. Unfortunately it is not well suited for doing analysis
and manipulation on it. Because of this there is a need to transform
syntax tree into another structure that would fill this need more
appropriately. This structure is abstract syntax tree. Abstract syntax
tree is designed for making analysis and manipulation on tree much easier.
AstCreateVisitor is responsible for converting syntax tree to abstract
syntax tree. Visitor visits each node of the syntax tree and produces
abstract syntax tree from it. For production specified in previous
section method looks like this:

\begin{minipage}{\linewidth}
\lstset{language=Java}
\begin{lstlisting}[
	breaklines=true, breakatwhitespace=true,
	prebreak= \space, postbreak= \space]
@Override
public DetailAst visitMultiplicativeExpressionDivRest(
		C11Parser.MultiplicativeExpressionDivRestContext ctx) {
   List<DetailAst> childrens = new LinkedList<DetailAst>();
   childrens.add(visit(ctx.multiplicativeExpression()));
   childrens.add(visit(ctx.castExpression()));
   return new DivRestExpressionAst(getText(ctx), childrens);
}
\end{lstlisting}
\end{minipage}

\section{Abstract syntax tree}
\label{sec:abstractSyntaxTree}
Abstract syntax tree is the convenient representation of the source code
used in doing analysis of the source code and manipulation. The base class
for this in JaPar is DetailAst.

It contains methods to recognize type of the node:

\begin{enumerate}
	\item is node loop
	\item is node control statement
	\item is node a compound statement
\end{enumerate}

It contains methods to make traversing the source code easy like:

\begin{enumerate}
	\item get children of node
	\item get first child of node
	\item get parent of node
\end{enumerate}

It contains few methods to find descendant of given type:

\begin{enumerate}
	\item find descendant nodes of given type 
	\item find descendant nodes that are loops
	\item find descendant nodes that are subscripts
\end{enumerate}

DetailAst is subclassed by more specific class that
represents specific node types.

\section{Calculating input and output dependency of a node}
Data Dependency analysis needs information about input and output
dependency for each node. DetailAst contains two methods
to calculate it:

\begin{enumerate}
	\item get input dependencies
	\item get output dependencies
\end{enumerate}

Algorithm for calculating input and output dependency is 
different for different nodes, each subclass of DetailAst is
overriding this function to provide node specific algorithm.

For simple case when node represent variable identifier input
dependency is variable that this node references and it has
not output dependency. 

Node that represents assignment in the form:

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}
int A,B;
A = B;
\end{lstlisting}
\end{minipage}

has as output dependency variable that is assigned(A variable in
this example), and input dependency of variable that is on the right
side of assignment(B variable in this example).

\section{Generating data flow graph}
\label{sec:GeneratingDataFlowGraph}
Data Flow Graph is main structure to determine dependencies between
statements. It contains information which variables is read in given
statement and what variables are generated as output. To generate
data flow graph we must know input and output dependencies of each
statement in source code. Generating data flow algorithm looks like this:

\begin{algorithm}
\caption{Generating Data Flow Graph}
\begin{algorithmic}[1]
\Procedure{GenerateDataFlowGraph}{$ast, varToLastUsageStmt$}
	\State {$ node = $ generate node for ast}
	\For{$var\leftarrow $ input $ \cup $ output dependencies of ast}
		\State {create edge between $ varToLastUsageStmt[var] $ and ast}
		\State {$ varToLastUsageStmt[var] = node $}
	\EndFor
	\For{$child\leftarrow $ children of ast}
		\State {$ GenerateDataFlowGraph(child, varToLastUsageStmt) $}
	\EndFor
\EndProcedure
\end{algorithmic}
\end{algorithm}


\section{Recognizing dependencies in Loop}
To be able to parallelize loop we have to make sure that there
are no dependencies between loop iterations and loop body is
not changing environment in which it is invoked. To do this
JaPar is doing following analysis of the loop body:


\begin{enumerate}
	\item Operation on Pointers
	\item Function invocations
	\item Usage of global variables
	\item Recognizing dependencies between array access
\end{enumerate}

\subsection{Operation on Pointers}
In general pointers operation is very hard to analyze. This is due to the
fact that with pointer manipulation we can access every part of
program memory. Current implementation checks if there is a pointer manipulation
and if it is recognized it marks loop as unparallelizable.

\subsection{Function invocations}
Function that is invoked inside a loop body can change environment in which
it is invoked. Environment consists not only of variables but of things
like files or screen as well. Current implementation allows only clean function
to be invoked inside a loop body. By clean function we mean function that
fulfill following requirements:

\begin{enumerate}
	\item Not doing pointer manipulations
	\item Not changing global variables
	\item Invokes functions that are clean as well, if 
	      function body can not be obtained in source(for
	      example it is located in third part library)
	      we assume that function is not clean
\end{enumerate}


\subsection{Usage of global variables}
When given loop is changing some global variable it is not easy
to check if loop is parallelizable. In general it is not, apart
from cases when we can do reduction on given variable. 


\subsection{Recognizing dependencies between array access}
At last we have to make sure that there are no dependencies
between array accessed. To do this we transform array access
to the system of equations and check if there are no solution
to this system of equations by running following algorithms:

\begin{enumerate}
	\item GCD
	\item Fourier Motzkin Elimination
	\item Barvinok Counting
	\item Range Test
\end{enumerate}

\section{Calculating data dependency vector}
\label{sec:calculatingDataDependencyVector}
Distance vector of dependency can be calculated using
Fourier-Motzkin elimination. Similarly as in the problem of
checking if there is a dependency between the instructions we are 
interested in information whether two data accesses $ f $ and $ g $
can access the same memory in two iterations $ i=(i_{1},i_{2},...,i_{n} $ and
$ i'=(i'_{1},i'_{2},...,i'_{n} $ but this time we are not only interested if
there is a dependency but what are the values of $ i' $ and $ i $ for which
there is a dependency.

Fourier-Motzkin works by eliminating all variables but one in 
input system of equations and checking if this system of equations can be solved.
If it can be, system of equations with eliminated rest of variables
states range of values for the last variable for which the input
system of equations can be solved. When we have the range of values
for a variable we can substitute this variable in input system
of equations with randomly chosen value from this range and run
Fourier-Motzkin once again to get the valid range for second variable.
We repeat this until all variables are eliminated. In this case
we have calculated all values for variables in iteration vectors 
$ i=(i_{1},i_{2},...,i_{n} $ and $ i'=(i'_{1},i'_{2},...,i'_{n}) $. 
In this point calculating dependency vector can be done by simple
substraction:

\begin{align}
	\begin{split}
		d = i' - i = (i'_{1}-i_{1}, i'_{2}-i_{2}, ..., i'_{n}-i_{n})
	\end{split}
\end{align}

This algorithm is shown in the following pseudocode:

\begin{algorithm}
\caption{Calculating data dependency vector between two statements}
\begin{algorithmic}[1]
\Procedure{CalculateDataDependencyVector}{$loopAst$, $ stmt1 $, $ stmt2 $}
	\State {$ variableValues $ = \{\}}
	\State {$ inputEquation = $ transform data accesses in $ stmt1 $ and $ stmt2 $ into system
		of equations under loop constraints in $ loopAst $}
	\While{$ inputEquation $ has variables}
		\State {$ outputEquation = $ Run Fourier-Motzkin and get output system of equation}
		\State {$ variable = $ Get non-eliminated variable from $ outputEquation $ }
		\State {$ variableValue = $ Take any value that satisfy $ outputEquation $}
		\State {Substitute $ variable $ by $ variableValue $ in $ inputEquation $}
		\State {Put tuple ($ variable $, $ variableValue $) into $ variableValues $}
	\EndWhile
	\State {$ dependencyVector = $ calculate dependency vector from $ variableValues $}
	\State \Return {$ dependencyVector $}
\EndProcedure
\end{algorithmic}
\end{algorithm}


\section{Parallelizing loops}
After analysis phase we have enough information to do parallelizing.
In the first phase we decide which transformation to run on loops.
To do this we use information about dependencies in loops and
dependency vectors. After this we do the transformations, the exact
steps that is done is dependent on programming language and library
used to make code run in parallel. For now there is implemented 
parallelizing invocation of C code using OpenMP library constructs
and using loop transformations. This chapter presents strategies for
optimizing and parallelizing loops according to the data from
analysis phase.

\subsection{Nested Loops}
When we parallelize loop that is nested inside another loop we
parallelize outer loop firstly. There are several reason to do
this:

\begin{itemize}
	\item Outer loop usually operates on larger blocks of data and
	      parallelizing it gives better speedups
	\item Inside loops tend to operate on much smaller blocks, so the
	      overhead of creating pool of threads adds too much overhead 
\end{itemize}

For example for given nested loop:

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}
for (i=0;i<=1000;i++) {
    for (j=0;j<=1000;j++) {
    	tab[i] = i + j;
    }
}	
\end{lstlisting}
\end{minipage}

Parallelization would insert parallel pragma before outer loop:

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}
#pragma omp parallel for
for (i=0;i<=1000;i++) {
    for (j=0;j<=1000;j++) {
    	tab[i] = i + j;
    }
}	
\end{lstlisting}
\end{minipage}

\subsection{Loops without data dependencies}
If the given loops has no data dependencies in it, the loop iterations
can be parallelized with the parallel for pragma, for example:

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}
int tab[1000];
for (i=0;i<=1000;i++) {
	tab[i] = 25*i + 4500;
}	
\end{lstlisting}
\end{minipage}

can be parallelized using parallel for pragma:

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}
int tab[1000];
#pragma omp parallel for
for (i=0;i<=1000;i++) {
	tab[i] = 25*i + 4500;
}	
\end{lstlisting}
\end{minipage}

\subsection{Loops changing global environment}
Sometimes the loop can change global environment and
still be parallelized. Another example is when the function 
that is invoked inside loop is not clean or we can not tell if
it is or not(for example when it is function from third
part library). In this cases we can use openmp critical directive
to force regions of code to be executed by only one thread
at a time.
\\
For example in this example we do not have a source to
reorganize_statistics function:

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}
int main() {
	for (i=0;i<=1000;i++) {
		tab[i] = 25*i + 4500;
		reorganize_statistics(tab);
	}	
}
\end{lstlisting}
\end{minipage}

To be able to safely parallelize it, we have to make sure that
only one thread is invoking this function at a time:

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}
int main() {
	#pragma omp parallel for
	for (i=0;i<=1000;i++) {
		tab[i] = 25*i + 4500;
		#pragma omp critical
		reorganize_statistics(tab);
	}	
}
\end{lstlisting}
\end{minipage}

\subsection{Loops with dependencies}
When loop carry dependency we use dependency vector
to reorganize loop to make it possible to run in
parallel. Dependency vector is specifying how many
iterations can run in parallel, for example dependency
vector $ [5] $ is stating that each element has dependency
on fifth previous element. This means that consecutive five
iterations does not have dependency on another iteration
in this group. To make it possible to parallelize, we divide
a loop with loop tiling to run operation in block with size
specified with dependency vector and parallelize output loop.

For a simple example:

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}
int main() {
	for (i=0;i<=1000;i++) {
		tab[i] = tab[i-5];
	}	
}
\end{lstlisting}
\end{minipage}

we do loop tiling with the block of five:

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}
int main() {
	for (k=0;k<=1000;k++) {
		for (i=k;i<=min(1000,k+5);i++) {
			tab[i] = tab[i-5];
		}	
	}
}
\end{lstlisting}
\end{minipage}

and parallelize inner loop:

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}
int main() {
	for (k=0;k<=1000;k++) {
		#pragma omp parallel for
		for (i=k;i<=min(1000,k+5);i++) {
			tab[i] = tab[i-5];
		}	
	}
}
\end{lstlisting}
\end{minipage}

\subsection{Loop Reduction}
There are cases when loops change variable in outer
scope in a matter that iterations can be parallelized
and the result obtained from results of each thread 
of execution combined in gives proper result. This
kind of parallelization is called reduction and OpenMP
is supporting this kind of parallelism with reduction
pragma. Formally

\begin{defn}
	Reduction of variable can be done when variable is manipulated 
	with given combination operator like:
	
	\begin{enumerate}
		\item +=
		\item -=
		\item *=
		\item /=
	\end{enumerate}
	
	and the result can be calculated by recombining its parts. In this
	case loop is parallelizable with given variable reduced.
\end{defn}

For example in this case:

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}
for (i=0;i<=100;i++) {
	sum += tab[i]*5;
}
\end{lstlisting}
\end{minipage}

can be parallelized by:

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}
#pragma omp parallel for reduction(+, sum)
for (i=0;i<=100;i++) {
	sum += tab[i]*5;
}
\end{lstlisting}
\end{minipage}

\subsection{Using information from memory analysis}
One of the information from code analysis is memory
analysis for loops. This data can be used to make
decision about parallelization. For example in OpenMP
it is good to state which variables are private for
given iteration and which are shared between iterations.
This information is important because according to this
information threads are allocating memory for private 
variables. This information is expressed as shared and
private option of parallel pragma. In the following example:

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}
for (k=0;k<NUM_LOOPS;k++) {
    #pragma omp parallel for shared(A, v, u, rows, columns) \
    			     private(i, j)
    for (i=0;i < rows;i++) {
        v[i] = 0.0f;
	for (j=0;j < columns; j++)
	    v[i] = v[i] + A[i][j] * u[j];
    }
}
\end{lstlisting}
\end{minipage}

It is declared that variables A, v, u, rows and columns are shared between
iteration threads and variables i and j are private to each iteration.




