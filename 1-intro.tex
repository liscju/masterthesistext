
\chapter{Preface}

\section{Motivation}

At present it is really seldom that a computer has only one
processor. Few years ago hardware manufacturers crossed
the line where speeding processor by minimizing digital
structures inside it became hard. One of the main reason
behind this is the digital parts became so small that further
speeding them up is limited by the speed of light. Because of
this a general tendency is to add more cores
or processors to the computer in order to get more processing.
\\
Unfortunately adding more cores to the
machine is not enough to speed it up. The application
must be appropriately designed to take advantage of this
additional power. What makes the matter worse is the fact that designing an 
application to run in parallel is not an easy task. To be
properly done it must be carefully crafted and tested. The
other disadvantage of designing application in such way
is code for parallel invocation of tasks is mangling between
normal functionalities. It makes the code harder to read and
modify. It would be pretty inconvenient if
programmer has to think about parallelizing every
functionality he writes. Modern compiler already has
abilities to parallelize application but it is much rarer
used than it should be. Obviously not all part of programs
are worth pararelizing so the main focus in this paper
is on the loops as they are places where the most
heavy weight computation takes place. Parallelizing loops is
especially crucial when implementing algorithms which operates
on large amount of data on large machines containing many
cores.
\\
As previously mentioned, there are many reasons why doing 
automatic parallelism is gaining significantly more attention,
many technics that are already implemented in compilers are
described in \cite{OptimizingCompilersForModernArchitectures}.
This paper presents an overview of methods to analyse and
transform program execution from sequential to parallel.

\section{Content of this work}

The chapter \ref{chap:problem-formulation} provides the
formulation of the problem and high level overview of
techniques used to overcome them. The chapter 
\ref{chap:methodology} outlines the tools and methods 
necessary for approaching the problems. In \ref{chap:loopformalism}
there is an introduction to formalism that is used to
describe loop analysis and transformation in strict
mathematical matter. Chapter \ref{chap:architecture}
presents the high level design of the software 
we developed to accomplish the research goals. Description of the problems that were chosen
to analyse,transform to parallel version and check the speedup
of their parallel version are described in \ref{chap:chosenProblems}.
In the chapter \ref{chap:docs} there is a documentation of the created application. 
I sum up the thesis in the chapter \ref{chap:conclusions}.

\section{State of the art}

\subsection{Yucca}

YUCCA \cite{MethodOfExtractingParallelizationInVeryLargeApplication}
is an automatic parallelization tool for projects 
written in C language. It provides source to source conversion,
on input it takes source code of the application written in
C and produce parallelized version of the source code as output.
YUCCA output is a multithreaded version of the input with Pthreads
or OpenMP constructs inserted at appropriate places. YUCCA uses
PThreads to do task parallelization and OpenMP to make loops
run in parallel. YUCCA consists of two main parts: front-end
which is responsible for parsing source code and back-end that
performs static dependency analysis to identify parts of code
that is worth to parallelised. YUCCA can be configured to
output dependency analysis report to the user in the form
of XML output.

\subsection{PLUTO}

PLUTO(\cite{PlutoPolyhedralAutomaticParallelizer})
is an automatic parallization tool based on polyhedral model.
Polyhedral model is the representation of programs, especially
parts of them involving loops and arrays as parametric polyhedra.
This representation provides an abstraction to perform high-level
transformations on the source code. PLUTO(similiarly to Yucca) 
is doing source to source transformation, that is takes as input
source code and produce source code as output.

It does coarse-grained parallelism at the same
time ensuring data locality. The fundamental transformation
works by finding affine transformations for efficient loop tiling.
Affine transformation is a function between affine spaces which
preserves points, straight lines and planes. PLUTO is doing
parallelization with OpenMP and the code is also transformed for
data locality. The tool provides number of options to tune aspects like 
tile sizes, unroll factors and outer loop fusion structure.

\subsection{PolyLib}
The Polyhedral Library(PolyLib) \cite{PolylibLibraryOfPolyhedralFunctions}
is a library for doing computations on object that consists of
unions of polyhedra. This library contains functions
for operation on those structures. It is mainly
used in parallelization domain where polyhedra is used
as convenient representation of nested loop iteration
structure. The good description of the Polylib is \cite{PolylibUserManual}

\section{Introduction to OpenMP} \label{sec:openmp-introduction}
OpenMP\cite{UsingOpenMP} is an API to support writing multithreaded 
applicaton. It is portable on the most popular hardware platforms
and operating systems. Additionally it and has a simple and flexible model 
that supports writing parallel application in the same way for desktops 
and supercomputers. Parallelizing application with OpenMP is done by 
inserting set of compiler directives and library routines. Compiler 
that supports OpenMP recognizes that constructs and the decision how many
threads of invocation will be responsible for calculating given
parts of code depends on the construction used and environment variables
values on run-time. 

Developer does not have to explicitly divide threads
into cores which greatly simplifies developing parallel code.
The developer's responsibility when creating parallel applications using
OpenMP can be divided into two activities.

In the first step developer identifies possible parallelism in the source code
of application. It is not always an easy task and in many cases
it involves transforming code to make parallelization possible.
This step can involve changing algorithm to the one that parallelizing
is more convenient.

In the second step he inserts OpenMP directives
before code which he wants to parallelise. Developer has
to decide which directive to use and to give them appropriate
options. OpenMP has only few directives but powerful enough to cover
even sophisticated needs.

With OpenMP developer does not have to parallelize application at once. 
He can incramentally recognize and parallelize application until needed 
speedup is reached. This makes developing parallel application much easier.

There are several different pragmas\cite{OpenMPDirectives}. These pragmas are
mainly for appling parallelism to loops (omp parallel, omp parallel for etc.), directives for data (shared or private data),
scheduling work between threads (schedule),  pragmas for efficient vectorization (simd, alligned) and 
synchronization between threads (locks, omp critical etc.)

\section{Parallel Computing Platforms}
Despite the fact that traditional microprocessor platform speed has
significantly improved over the past decade it has also 
few performance bottlenecks. To overcome those limitation
and achieve best performance there were proposed parallel
computing platforms created to utilize the power of
hardware by pararelizing computations.

\subsection{Multi-Core Processors}
Processors in a modern machine has two or more independent central
processing units to make it possible to run multiple CPU instructions
at the same time\cite{ComputerArchitectureAQuantitiveApproach}. 
Each of these units are integrated in the single chip. Main manufacturers of processors
(AMD and Intel) have created multiple multi-core processor designs.
They are different in multiple ways:

\begin{itemize}
	\item cores may or may not share caches
	\item core can communicate with another using message passing
	      or using shared memory
	\item cores can support vector processing
	\item cores can support SIMD
	\item cores can support multithreading
\end{itemize}

Most of the processors used in todays PCs are multi-core, just to
name a few:

\begin{itemize}
	\item AMD - Athlon 64, Athlon 64 FX, Athlon II, Opteron
	\item Intel - Core 2 Duo, Itanium 2, Pentium D, Xeon, Xeon Phi
	\item Sun Microsystems SPARC T4
\end{itemize}

One of the most popular Multi-core processor architecture is
fitting all cores on a single processor socket (sometimes called
Chip Multi-Processor). The architecture of this solution is presented
in the following diagram:

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.75]{./Diagrams/multicorearchitecture.eps}
	\caption{Multi-core CPU chip}
\end{figure}

Regardless of techniques supported by cores processor offer different
types of parallelism. However performance gained by using multi-core
machine largely depends on algorithms used to implement programs and architecture of the parallel machine (hierarchy of memory
clock frequency, communication between parallel cores, access to the memories etc.).
To support programming multi-core machines many
models and libraries were created. One of the most popular libraries to support
programming multi-core machines are Pthreads, OpenMP, Cilk++, TBB etc.

\subsection{GPGPU}
Another example of very popular parallel computing platform is GPGPU\cite{ParallelImplementationOfSpatialPoolerInHierarchicalTemporalMemory}.
It is built by integrating many multiprocessor in the single platform.
Each multiprocessor may contain many cores. Multiprocessors
have special memory chips which are shared among all multiprocessors.
Those dedicated memory chips are much faster than global memory. There
are two main types of those memory chips:

\begin{itemize}
	\item real-only constant/texture memory 
	\item shared memory 
\end{itemize}

The GPGPU cards enables even thousands of parallel threads to run.
Threads are grouped in blocks with shared memory. To support programming
GPGPU system there is a dedicated software architecture called CUDA. 
CUDA is programmed using high-level languages such as C and C++. Main
mechanism to parallelize program that CUDA provides are:

\begin{itemize}
	\item thread group hierarchy 
	\item shared memories
	\item synchronization with barriers 
\end{itemize}

Examples of the popular GPGPU cards are the following:

\begin{itemize}
	\item AMD Radeon and FireStream
	\item NVidia GeForce 9, GeForce 200, Tesla
\end{itemize}

Important aspect of programming CUDA is optimizing synchronization
and communication of the threads. Threads in blocks synchronize
much faster than between blocks, so to gain best performance
communication between blocks should be kept to minimum. More information
about CUDA can be found on its website \cite{CudaWebsite}. AMD
GPGPU use other parallel programming platform called OpenCL - more
can be found in \cite{AMDOpenCLWebsite}.

\section{Main thesis of this work}

This paper focuses on showing current possibilities for extracting parallelism
in programs. We especially focus on methods to do it automatically on
source code. This methodology has several advantages. First of all, not all
developers have sufficient knowledge how to parallelize application. Secondly,
parts of code that parallelise is making code much harder to read
and modify. Thirdly, manual parallelization is a hard and error-prone
process. Even good developers do not always see potential of doing
parallelism.

However doing automatic parallelism is very difficult for many reasons.
It is hard to properly detect dependencies in code that uses pointers
or recursion. It is not always possible to do full analysis statically.
Loop bounds can depend on parameters unknown at compile time, e.g.,
when loop bounds depend on variable given from program execution environment.
Global resource accesses makes this task even more complicated because
it has to be coordinated between threads of execution.

Additionally this paper shows how we can represent information about
loop using polyhedra model. Polyhedra model is a mathematical representation
of iteration of loops as lattice points inside polytopes. This model
is very convenient because mapping from nested loops to polhedra
is natural. There are many algorithms to do calculation on polytopes
which are very useful in doing dependency analysis.

In order to describe loop analysis and loop transformation in strict
mathematical manner, this paper introduces new formalism and notation
to reason about loops. Transformations of loops are presented as mapping
between objects in this formalism.

This paper introduces JaPar - a new tool for automatically pararelizing
program invocations. The main idea behind working on the new tool was making
it possible to make one parallelization tool that supports many languages and
platform. This tool is divided into three separate parts: 

\begin{enumerate}
	\item Front End - responsible for parsing source code
	\item Intermadiate Form Analysis - responsible for Abstract Syntax Tree analysis 
	\item Back End - responsible for doing transformation on source code
\end{enumerate}

This makes it easier to exchange parts for different languges and parallelization
techniques. For example we can use Analysis part of the system to analysis
the possibility of parallization for different front ends and back ends
for different computer languages like java, scala and for different parallel
programming frameworks like openmp, Java Threads.



