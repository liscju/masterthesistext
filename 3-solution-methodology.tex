
\chapter{Methodology of the solution} \label{chap:methodology}

One of the most important process in developing automatic parallelization
tool is data dependence analysis. This analysis enables compiler
to identify code fragments that can be executed in parallel. There
are already many methods proposed, some of them are presented
in this chapter, particular emphasis is placed on methods used in
created prototype. Each of the proposed dependency test algorithm has different trade-off
between accuracy and computational complexity to calculate it.

\section{Polyhedral Model}
The polyhedral model\cite{PolyhedralInfo} is a powerful mathematical formalism used
extensively in compilers and optimization domain. It is used
extensively in variety of contexts:

\begin{enumerate}
	\item automatic parallelization
	\item program verification
	\item cache analysis
	\item data locality
\end{enumerate}

The formal definition of polyhedron is the following:

\begin{defn}
	A Polyhedron is a set $ P = \{ x \in R^{n}: Ax <= b \}$
	where $ A $ is a $ m \times n $ matrix, and b a $ m \times 1 $ vector.
\end{defn}

Polyhedron model is well suited to model things like loop iterations.
The iteration in the following loop:

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}
for (i=0;i<=3;i++) {
	for (j=0;j<=i;j++) {
		...
	}
}
\end{lstlisting}
\end{minipage}

can be translated to the following polyhedron:

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.5]{./Diagrams/PolyhedronExample.eps}
	\caption{Translated loop iterations to polyhedron}
\end{figure}

\section{Data Dependency Problem}

The data dependence\cite{AdvancingDataDependenceAnalysisInPractice} 
is defined between two statements in the 
source code that access same memory address. In particular, there is
a dependence relation between those statements if one statement
performs write operation in the same memory location that other
statement reads or write. Data dependency between two instructions
is denoted by dependency relation, such as:

\begin{align}
	\begin{split}
		S1\ Q_{<}\ S2
	\end{split}
\end{align}

where $S1$, $S2$ are statements and $ Q_{<} $ is relation between
those statements. Relation $ Q_{<} $ holds between $ S1 $ and 
$ S2 $ if for example $ S1 $ writes and $ S2 $ reads from the same physical location.

For example for the following code:

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}
int i = 5;
int k = i + 10;
\end{lstlisting}
\end{minipage}

$S1$ is the first line, $S2$ is the second line and relation between them
is $ S1 Q_{<} S2$ and variable that causes the dependency is $i$

There are three types of data dependence:

\begin{enumerate}
	\item Flow dependence
	\item Anti dependence
	\item Output dependence
\end{enumerate}

\subsection{Flow dependence}
Flow dependence between statement S1 and S2 is a situation when
S1 writes on a memory location that S2 reads later. For example:

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}
int i = 5;
int k = i + 10;
\end{lstlisting}
\end{minipage}

Those two statements has flow dependence on $ i $ variable. 

\subsection{Anti dependence}

Anti-dependence between S1 and S2 is happening when S1 reads a 
memory location and S2 writes on this particular memory
location later. For example:

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}
int i = w;
w = 10;
\end{lstlisting}
\end{minipage}

Has anti-dependence between those two statements on $ w $ variable.

\subsection{Output dependence}

Output dependence happens when statement S1 writes on a memory location
first and S2 writes on the same memory location later, for example:

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}
w = 30;
w = 10;
\end{lstlisting}
\end{minipage}

Has output dependence on $ w $ variable.

\section{Data Flow Analysis} 
\label{sec:dataFlowAnalysis}
Data flow analysis is used to gather information about dynamic behavior
of a computer program by doing static or dynamic analysis of the source code. 
Information obtained in data flow analysis is used in data dependency
analysis. One of the most important data structure used in data flow 
analysis is data flow graph. 

This graph contains nodes that represents
instructions of the program and edges that represents dependencies between
instructions. Each edge contains information about variables that are
responsible for dependency and dependency type. The dependencies between
instructions can be one of the following types: flow dependence, anti-dependence
and output dependence. Building Data Flow Graph starts with calculating
input and output dependencies for each statement in the program. Input dependencies
are variables that are read in statement, whereas output dependencies
are variables are are written in statement. From each statement in the program
we produce a node in Data Flow Graph with information about dependencies.
After creating nodes in data flow graph, we connect them by edges. Each
two nodes are connected by edge if and only if there exist a variable that
both nodes depends on.

For example for a given source code:

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}[label={lst:datadependencyloop}]
void segregate0and1(int array[], int size) {
   int left = 0, right = size-1;
   while (left < right) {
   	while(array[left] == 0 && left < right)
   	   left++;

   	while (array[right] == 1 && left < right)
   	   right--;

   	if (left < right) {
   	   array[left] = 0;
   	   array[right] = 1;
   	   left++;
   	   right--;
   	}
   }
}
\end{lstlisting}
\end{minipage}

the following data flow graph is generated:

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.5]{./Diagrams/testCaseArrayInLoops.png}
	\caption{Example for Data Flow graph}
\end{figure}

Nodes in this graph are represented by circles and edges by
arrows. Each edge has additional information about dependency type and
variables. Dependency types are following:

\begin{enumerate}
	\item RAR: Read After Read
	\item WAR: Write After Read
	\item RAW: Read After Write
	\item WAW: Write After Write
\end{enumerate}

Edges that are red are backward edges. Backward edges are used
to determine dependencies between loop iterations where statement
has dependency on the instruction that is located before this statement
in the loop.

Data Flow Graph is used in many places when analyzing program
dependency. First of all Data Flow Graph makes easy to check
if loop is not writing to the variable from output scope. It makes
it easy to check if there are arrays used in iterations. 

\subsection{Data Dependence in Loops}
\label{subsec:dataDependenceInLoops}
Data dependencies outside loops\cite{DataDependencyTestingInPractice}
are between instructions that are located
before and after loops in source code. Dependencies between instructions that
are part of loop body are more complicated because each statement in a loop can contain array type
variable with subscript expression. This determines that subscripts of the same array in two different
statements can have same values and cause the dependency (inner or between iterations). 

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}[label={lst:datadependencyloop}]
for (I1 = P1;I1 <= Q1;I1++) {
   for (I2 = P2;I2 <= Q2;I2++) {
      ...
      for (In = Pn;In <= Qn;In++) {
         A1[f1(I1, I2, .., In)] = ... A1[g1(I1, I2, .., In)]
         A2[f2(I1, I2, .., In)] = ... A2[g2(I1, I2, .., In)]
         ...
         Ap[fp(I1, I2, .., In)] = ... Ap[gp(I1, I2, .., In)]
      }
   }
}
\end{lstlisting}
\end{minipage}

Data dependency relation\cite{DataDependencyTestingInPractice}
between two statements exist when they access
same memory location. For variables this means that two statements
read or write given variable. For arrays it is more complicated because
two statements that read or write to given array do not necessarily
access the same memory location. Things get even more complicated if
array access is part of loop body. This means that access to the same
memory location can be in different iterations of the loop. To check
if some iterations of the loop access the same memory location in array
we model this situation with system of equation. Two array references
access the same memory location when expressions in array index has the
same value. To the following system of equations we add constraints from
loop indices. The following loop \ref{lst:datadependencyloop} can be
translated to the following system of equations:

\begin{align}
	\label{eq:datadependencysystemofequation}
	\begin{split}
	f_{1}(I_{1}, I_{2}, I_{3}, .., I_{n}) = g_{1}(I\prime_{1}, I\prime_{2}, I\prime_{3}, .., I\prime_{n}) \\
	f_{2}(I_{1}, I_{2}, I_{3}, .., I_{n}) = g_{2}(I\prime_{1}, I\prime_{2}, I\prime_{3}, .., I\prime_{n}) \\
	...                                                                                                   \\
	f_{p}(I_{1}, I_{2}, I_{3}, .., I_{n}) = g_{p}(I\prime_{1}, I\prime_{2}, I\prime_{3}, .., I\prime_{n}) \\
	\end{split}
\end{align}

To this system of equations we have to add loop index constraints:


\begin{align}
	\label{eq:datadependencysystemofequationconstraint}
	\begin{split}
		P_{k} <= I_{k} <= Q_{k}, P_{k} <= I\prime_{k} <= Q_{k}, 1 <= k <= n \\
	\end{split}
\end{align}

$ P_{k} $ and $ Q_{k} $ represents the lower and upper bounds
of appropriate loop index. If the system of equations constructed
from \ref{eq:datadependencysystemofequation} and 
\ref{eq:datadependencysystemofequationconstraint} has an integer solution
then there exist loop iterations that access the same memory location.
This means that there exist data dependency between instructions.
The problem is typical linear integer programming problem. There exist solution that run in polynomial
time for certain class of instances of the problem. In literature there
are number of solutions proposed to this problem[]. It is worth to note that
data dependence tests approximate on conservative side. This means that we
assume that dependency exist if independence cannot be proved. This
assumption is crucial because in other case we would not be sure
that parallelizing given loop leads to a valid program.

\subsection{Fourier Motzkin Elimination}
As we seen in previous section we can translate
the following loop array accesses:

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}[label={lst:datadependencyloop}]
for (I := 1;I <= 100;I++) {
   X[I] = ..X[I-1]...
}
\end{lstlisting}
\end{minipage}

To the following system of equations:


\begin{align}
	\label{eq:datadependencysystemofequationexample}
	\begin{split}
		1 <= I_{w} <= 100   \\
		1 <= I_{r} <= 100   \\
		I_{w} = I_{r} - 1   \\
	\end{split}
\end{align}

Variables $ I_{w}, I_{r} $ are representing different loop
iteration that accesses array.

Calculating if there is dependency between assignment statement in
two different loop iteration is reduced to solving the system of equation.
One of the most popular methods to do this is Fourier-Motzkin elimination.
Fourier-Motzkin elimination works by eliminating variables from a system
of linear inequalities. After eliminating all variables system of equations
contains only constant inequalities. It makes it trivial to decide whether
the resulting system has solution or not. 
In \cite{FourierMotzkinWithNonLinearSymbolicConstantCoefficients} the 
Fourier-Motzkin Elimination is described as the following - for the general
system of equation:

\begin{align}
	\begin{split}
		a_{1,1}*x_{1} + a_{1,2}*x_{2} + ... + a_{1,n}*x_{n} + b_{1} >= 0 \\
		a_{2,1}*x_{1} + a_{2,2}*x_{2} + ... + a_{2,n}*x_{n} + b_{2} >= 0 \\
		....                                                             \\
		a_{m,1}*x_{1} + a_{m,2}*x_{2} + ... + a_{m,n}*x_{n} + b_{m} >= 0 \\
	\end{split}
\end{align}

We can obtain equivalent system of equation by multiplying each equation
by $ 1/|a_{i, 1}| $(assuming it is non-zero value) and grouping resulted
inequalities by sign of the coefficient of $ x_{1} $:

\begin{align}
	\begin{split}
		x_{1} + a'_{i,2}*x_{2} + ... + a'_{i, n}*x_{n} + b'_{1} >= 0   (i \in S^{+})  \\
		a_{i,2}*x_{2} + ... + a_{i,n}*x_{n} + b_{i} >= 0               (i \in S^{0})  \\
		-x_{1} + a'_{i,2}*x_{2} + ... + a'_{i, n}*x_{n} + b'_{i} >= 0   (i \in S^{-})  \\
	\end{split}
\end{align}

where:

\begin{align}
	\begin{split}
		S_{+} = \{i | a_{i, 1} > 0\} \\
		S_{0} = \{i | a_{i, 1} = 0\} \\
		S_{-} = \{i | a_{i, 1} < 0\} \\
		a'_{i,j} = a_{i, j}/|a_{i, 1}| \\
		b'_{i} = b_{i}/|a_{i,1}| \\
	\end{split}
\end{align}
$ x_{1},x_{2},x_{3},..,x_{n} $ is a solution to the original system if and only if
$ x_{2},x_{3},..,x_{n} $ satisfies:

\begin{align}
	\begin{split}
		\sum\limits_{j=2}^{n}{a'_{k,j}*x_{j}} + b'_{k} + 
		\sum\limits_{j=2}^{n}{a'_{i,j}*x_{j}} + b'_{i} >= 0 (i \in S^{+}, k \in S^{-}) \\
		\sum\limits_{j=2}^{n}{a'_{i,j}*x_{j}} + b'_{k} >= 0 (i \in S^{0}) \\
	\end{split}
\end{align}

and $ x_{1} $ satisfies:

\begin{align}
	\begin{split}
		\underset{i \in S_{+}}{max}(\sum\limits_{j=2}^{n}{-a'_{i,j}*x_{j}} - b'_{i})
		<= x_{1} <= 
		\underset{k \in S_{-}}{min}(\sum\limits_{j=2}^{n}{a'_{k,j}*x_{j}} + b'_{k})
	\end{split}
\end{align}

After this projection we eliminated one variable and we proceed similarly to eliminate
$ x_{2}, x_{3}, ..., x_{n}$.

\subsection{Fourier-Motzkin with Non-Linear Symbolic Constant Coefficients}
In paper \cite{FourierMotzkinWithNonLinearSymbolicConstantCoefficients} there is
a proposal of an extension to Fourier Motzkin to handle non-linear system with 
symbolic constant coefficients. In standard Fourier-Motzkin all
coefficients $ a_{i,j} $ in the inequalities of the form:

\begin{align}
	\begin{split}
		a_{i,1}*x_{1} + a_{i,2}*x_{2} + ... + a_{1,n}*x_{n} + b_{i} >= 0 \\
	\end{split}
\end{align}

has to be constant. In paper \cite{FourierMotzkinWithNonLinearSymbolicConstantCoefficients}
it is proposed to have all inequalities in the form presented below:

\begin{align}
	\begin{split}
		f(\overrightarrow{u},\overrightarrow{v}) = 
		p_{0}(\overrightarrow{u}) + p_{1}(\overrightarrow{u})*v_{1} +
		p_{2}(\overrightarrow{u})*v_{2} + ... + 
		p_{n}(\overrightarrow{u})*v_{n} >= 0
	\end{split}
\end{align}

where $ \overrightarrow{v} = (v_{1},v_{2},...,v_{n}) $ are integer variables
and 
$ p_{0}(\overrightarrow{u}),p_{1}(\overrightarrow{u}),...,p_{n}(\overrightarrow{u}) $
are arbitraty integer polynomials in the symbolic constants 
$ \overrightarrow{u} = (u_{1},u_{2},...,u_{n}) $. Specifically,

\begin{align}
	\begin{split}
		\forall{x(0 <= x <= n)}p_{x}(\overrightarrow{u}) = 
			\sum_{i=0}^{}c_{x,i}\prod_{j=1}^{m}u_{x,j}^{z_{x,ij}}			
	\end{split}
\end{align}

where $ c_{x,i} $ is integer constant and $ z_{x,ij} $ is the polynomial power of
the symbolic constant $ u_{x,j} $.

Taking into consideration the following system of inequalities:

\begin{align}
	\begin{split}
		f^{1}(\overrightarrow{u}, \overrightarrow{v}) = 
			p_{0}^{1}(\overrightarrow{u}) + 
			p_{1}^{1}(\overrightarrow{u})*v_{1} +
			+ p_{2}^{1}(\overrightarrow{u})*v_{2} + ... + 
			p_{n}^{1}(\overrightarrow{u})*v_{n} >= 0 \\
		f^{2}(\overrightarrow{u}, \overrightarrow{v}) = 
			p_{0}^{2}(\overrightarrow{u}) + 
			p_{1}^{2}(\overrightarrow{u})*v_{1} +
			+ p_{2}^{2}(\overrightarrow{u})*v_{2} + ... + 
			p_{n}^{2}(\overrightarrow{u})*v_{n} >= 0 \\
		..... \\
		f^{r}(\overrightarrow{u}, \overrightarrow{v}) = 
			p_{0}^{r}(\overrightarrow{u}) + 
			p_{1}^{r}(\overrightarrow{u})*v_{1} +
			+ p_{2}^{r}(\overrightarrow{u})*v_{2} + ... + 
			p_{n}^{r}(\overrightarrow{u})*v_{n} >= 0 \\
	\end{split}
\end{align}

where $ r $ is the total number of inequalities in the system. 

Suppose that we are eliminating the variable $ v_{x} $.
In the first step we separate the inequality constraints into four sets: 
$ S_{x}^{+}, S_{x}^{0}, S_{x}^{-}, S_{x}^{?} $. 

$ S_{x}^{0} $ contains any inequalities not involving $ v_{x} $, i.e.

\begin{align}
	\begin{split}
		S_{x}^{0} = \{ f^{y}(\overrightarrow{u},\overrightarrow{v}) \mid
			0 <= y <= r, p_{x}^{y} = 0 \}
	\end{split}
\end{align}

$ S_{x}^{+} $ contains any inequalities which coefficient values of $ v_{x} $ are
positive, i.e.

\begin{align}
	\begin{split}
		S_{x}^{+} = \{ f^{y}(\overrightarrow{u},\overrightarrow{v}) \mid
			0 <= y <= r, p_{x}^{y} > 0 \}
	\end{split}
\end{align}

$ S_{x}^{-} $ contains any inequalities which coefficient values of $ v_{x} $ are
positive, i.e.

\begin{align}
	\begin{split}
		S_{x}^{+} = \{ f^{y}(\overrightarrow{u},\overrightarrow{v}) \mid
			0 <= y <= r, p_{x}^{y} < 0 \}
	\end{split}
\end{align}

And finally, $ S_{x}^{?} $ contains any inequalities in which coefficient values of
$ v_{x} $ are undetermined.

Additionally to the four sets we also have a context D which contains partial 
domain knowledge of the symbolic constant values. More formally the context
consists of inequalities of the form:

\begin{align}
	\begin{split}
		g(\overrightarrow{u}) = 
			\sum_{i=0}^{}c_{i}\prod_{j=1}^{m}u_{j}^{z_{ij}}
	\end{split}
\end{align}

where $ \overrightarrow{u} = (u_{1},u_{2},...,u_{n}) $ are symbolic constants,
$ c_{i} $ is integer constant and $ z_{j} $ is the polynomial power of the
symbolic constant $ u_{j} $. \\

Given $ S_{x}^{+}, S_{x}^{0}, S_{x}^{-}, S_{x}^{?} $ and $ D $ we proceed as
follows. In linear Fourier-Motzkin to eliminate $ v_{x} $ from the 
inequality constraints we have to take pair of constraints with opposite 
coefficient values of  $ v_{x} $ and compute their positive combination
(with appropriate positive constant multipliers) in order to create a new 
inequality constraint which does not involve $ v_{x} $. Because the value of 
$ p_{x}^{y}(0 <= y <= r \land f^{y}(\overrightarrow{u}, \overrightarrow{v})) \in S_{x}^{?} $
is undetermined, for every inequality in $ S_{x}^{?} $, we recursively branch
on the current system into the following cases 

\begin{enumerate}
	\item $ p_{x}^{y} = 0 $
	\item $ p_{x}^{y} > 0 $
	\item $ p_{x}^{y} < 0 $
\end{enumerate}

When $ p_{x}^{y} = 0 $, we move the inequality from $ S_{x}^{?} $ to 
$ S_{x}^{0} $. In the same way if $ p_{x}^{y} > 0 $ or $ p_{x}^{y} < 0 $,
we move the inequality from $ S_{x}^{?} $ to $ S_{x}^{+} $ or $ S_{x}^{-} $ 
respectively. \\

Each branch introduces a new set of linear constraints on the symbolic parameters to
the context. 

For example when we consider the following loop:

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}
for(i=0;i<=10;i++) {
    tab[i] = tab[N*i+15];
}
\end{lstlisting}
\end{minipage}

and add domain information that $ N >= 0 $ (this information can be known
for example when N has type unsigned in C) we have the following
system of equation:

\begin{align}
	\begin{split}
		i_{1} >= 0 \\
		-i_{1} + 10 >= 0 \\
		i_{2} >= 0 \\
		-i_{2} + 10 >= 0 \\
		i_{1} - N * i_{2} - 15 >= 0 \\
		-i_{1} + N * i_{2} + 15 >= 0 \\
		N >= 0
	\end{split}
\end{align}

We want to eliminate $ i_{1} $ variable. At first we calculate
$ S_{x}^{+}, S_{x}^{0}, S_{x}^{-}, S_{x}^{?} $ sets:

\begin{align}
	\begin{split}
		S_{x}^{+} = \{ 1, 5 \} \\
		S_{x}^{-} = \{ 2, 6 \} \\
		S_{x}^{0} = \{ 3, 4, 7 \} \\
	\end{split}
\end{align}

There is no $ S_{x}^{?} $ set because all coefficient of
$ i_{1} $ are constant values. Now we compute positive combinations
of equations in $ S_{x}^{+} $ and $ S_{x}^{-} $ and add equations 
$ S_{x}^{0} $. From this we obtain the following system of equation:

\begin{align}
	\begin{split}
		i_{2} >= 0 \\
		-i_{2} + 10 >= 0 \\
		N*i_{2} + 15 >= 0 \\
		-N*i_{2} - 5 >= 0 \\
		N >= 0
	\end{split}
\end{align}

Now we want to eliminate $ v_{2} $ variable. To do this we calculate 
$ S_{x}^{+}, S_{x}^{0}, S_{x}^{-}, S_{x}^{?} $ sets:

\begin{align}
	\begin{split}
		S_{x}^{+} = \{ 1 \} \\
		S_{x}^{-} = \{ 2 \} \\
		S_{x}^{0} = \{ 5 \} \\
		S_{x}^{?} = \{ 3, 4 \} \\
	\end{split}
\end{align}

We have two inequalities in $ S_{x}^{?} $. For both of this inequalities
$ N $ coefficient is undetermined, so we do not know if coefficient
beside $ i_{2} $ is positive, negative or zero. We have to recursively check
three possibilities:

\begin{enumerate}
	\item $ N = 0 $
	\item $ N > 0 $
	\item $ N < 0 $
\end{enumerate}

For each of this possibilities we have to determine
$ S_{x}^{+}, S_{x}^{0}, S_{x}^{-} $ and proceed with computation as usual.
For example when $ N > 0 $ those sets are defined as following:

\begin{align}
	\begin{split}
		S_{x}^{+} = \{ 1, 3 \} \\
		S_{x}^{-} = \{ 2, 4 \} \\
		S_{x}^{0} = \{ 5 \} \\
	\end{split}
\end{align}

Now we can proceed with computation as usual.

\subsection{The Banerjee Test}
The Banerjee test is another example of popular data dependence test.
It is easy to understand and has straightforward and efficient
implementation. The Banerjee test is described in \cite{DataDependenceAndItsApplicationToParallelProcessing}.
The general idea behind its working is intermediate
value theorem. It states that if a continuous function f with an interval
$ \in [a,b] $ takes values $ f(a) $ and $ f(b) $ at each end of the
interval, then it also takes any value between $ f(a) $ and $ f(b) $
at some point within the interval. For example for the following
function:

\begin{align}
	\begin{split}
		f(x) = x^2, x \in \Re \subset \Re^n
	\end{split}
\end{align}

and interval $ [0, 5] $ there exist x for all values y in the
range $ [f(0), f(5)]$ such that $ f(x) = y $

The intermediate value theorem can also be used to test if
linear equation has no solutions. For example for the following
linear equation:

\begin{align}
	\begin{split}
		a_{1}x_{1} + a_{2}x_{2} + ... + a_{n}x_{n} = a_{0} \\
		P_{k} <= x_{k} <= Q_{k}, for 1<=k<=n               \\
	\end{split}
\end{align}

The minimum and maximum values of the land-hand expression can be 
determined as


\begin{align}
	\begin{split}
		L = \sum_{k=1}^{n}(\overset{+}{a_{k}}P_{k}-\overset{-}{a_{k}}Q_{k}) \\
		U = \sum_{k=1}^{n}(\overset{+}{a_{k}}Q_{k}-\overset{-}{a_{k}}P_{k}) \\
	\end{split}
\end{align}

where

\begin{align}
	\begin{split}
		\overset{+}{a} = 
			\begin{cases}
				a & \quad \text{if } a > 0 \\
				0 & \quad \text{otherwise}
			\end{cases}
	\end{split}
\end{align}

and


\begin{align}
	\begin{split}
		\overset{-}{a} = 
			\begin{cases}
				-a & \quad \text{if } a < 0 \\
				0 & \quad \text{otherwise}
			\end{cases}
	\end{split}
\end{align}

When $ L > a_{0} $ or $ U < a_{0} $ the Banerjee test determine
that equation has no real solution, so in the result it cannot
have integer solution as well. In other case when $ L <= a_{0} <= U $
the Banerjee test cannot determine if equation has solution.

\subsection{GCD Test}
The GCD test is probably the most basic dependency test. It is
often used before more sophisticated test because it is
very efficient. GCD test is based on the following theorem:

\begin{theorem}
	The Diophantine equation $ ax + by = c $ ,where x and y are integers,
	if and only if $c$ is a multiple of the greatest common divisor of
	$a$ and $b$ 
\end{theorem}

For example for the following loop:

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}[]
for (I = 1;I <= 100;I++) {
   tab[3*I+4] = tab[6*I + 8];
}
\end{lstlisting}
\end{minipage}

we check if dependency exist between array access with the
following equation:

\begin{align}
	\begin{split}
		3*i_{w} + 4 = 6*i_{r} + 8
	\end{split}
\end{align}

which can be rewritten to:

\begin{align}
	\begin{split}
		3*i_{w} - 6*i_{r} = 4
	\end{split}
\end{align}

Greatest common divisor of 3 and 6 equals 3 which does not divide 4
so the loop has no carried dependency.

GCD test is incapable of proving dependencies, it only can disprove that
solution exists. This means that we cannot use GCD test for calculating
dependency vectors. Another thing is GCD can state that equation has no solution
for very limited cases because it is very likely that at least one of the
coefficient is equal 1. This makes greatest common divisor of coefficients
equals one and in the result $c$ is always multiple of the greatest common
divisor.

\subsection{Range Test}

In paper \cite{TheRangeTestADependenceTestForSymbolNonLinearExpr} 
data dependency test that handles loop bounds
or array subscripts that are symbolic, nonlinear expressions - 
The Range Test - is introduced. In the range test to prove 
that two array references do not access the same memory we prove
that the range of elements that can be accessed by first array access 
expression do not overlap with second array access expression.
This can be done by comparing minimum and maximum values that
given array access can obtain. The formal definition 
\cite{TheRangeTestADependenceTestForSymbolNonLinearExpr}
are defined below:

\begin{defn}
	Let $ f_{j}^{min}(i_{1},...,i_{j}) $ and 
	$ f_{j}^{max}(i_{1},...,i_{j}) $ be functions that
	obey the following constraints:

	\begin{align}
		\begin{split}
			f_{j}^{min}(i_{1},...,i_{j}) <= 
			min \{ f(\overrightarrow{i}'): \overrightarrow{i}' \in 
			\Re, i_{1}' = i_{1},..., i_{j}' = i_{j} \} 
		\end{split}
	\end{align}

	\begin{align}
		\begin{split}
			f_{j}^{max}(i_{1},...,i_{j}) >= 
			max \{ f(\overrightarrow{i}'): \overrightarrow{i}' \in 
			\Re, i_{1}' = i_{1},..., i_{j}' = i_{j} \} 
		\end{split}
	\end{align}
\end{defn}

The ability to determine the minimum or maximum of $ f $
or $ g $ in respect to some set of loops leads us to our first
dependence test.

First dependence test is based on our ability to determine the 
minimum or maximum values of $ f $ and $ g $. If the maximum of 
one function is less than the minimum of the other (for some subset 
of loop), then these loops are not dependent. More formally:
\cite{TheRangeTestADependenceTestForSymbolNonLinearExpr}

\begin{theorem}
	If $ f_{j}^{max}(i_{1},...,i_{j}) <= 
	g_{j}^{min}(i_{1},...,i_{j}) $ for all
	$ i_{1},...,i_{j} \in \Re_{j} $, then there
	can be no dependencies between $ A(f(\overrightarrow{i})) $
	and $ A(g(\overrightarrow{i})) $ with a direction vector
	$ \overrightarrow{d} $ of the form
	$ d_{1} = '=',...,d_{j} = '='  $
\end{theorem}

Theorem states that there are no dependencies between
$ A(f(\overrightarrow{i})) $ and $ A(g(\overrightarrow{i})) $
for indices $ i_{j+1},...,i_{n} $, if set of
possible values taken by one function does not overlap
with the range of possible values taken by the other function. 
This case can be presented in the following diagram:

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.5]{./Diagrams/RangeTestExample1.eps}
	\caption{$ f(i,j) = i $, $ g(i, j) = i + N $}
\end{figure}

Unfortunately, with this theorem we cannot prove that there are
no dependencies when possible values taken by $ f $ and $ g $
interlave.

The next theorem can disprove carried dependencies
for interleaving where the possible values
taken by functions for a single iteration are not
interleaving with the possible values taken by iteration. 

To describe this test, we must define the property of monotonicity 
for a particular loop index.

\begin{defn}
	A function $ f(\overrightarrow{i}) $ is monotonically
	non-descreasing for index $ i_{j} $ iff 
	\\ $ f(i_{1},...,\alpha_{j},...,i_{n}) <= f(i_{1},...,\beta_{j},...,i_{n}) $
	whenever $ 0 <= \alpha_{j} <= \beta_{j} <= N_{j} $.
	A function $ f(\overrightarrow{i}) $ is monotonically
	non-increasing for index $ i_{j} $ iff 
	$ f(i_{1},...,\alpha_{j},...,i_{n}) >= f(i_{1},...,\beta_{j},...,i_{n}) $
	whenever $ 0 <= \alpha_{j} <= \beta_{j} <= N_{j} $.
\end{defn}

Next two theorem states how to disprove dependencies carried at
certain level of loop nest $ j $ when the possible values taken
by two functions $ f $ and $ g $ are not interleaving for a single
iteration of the loop:

\begin{theorem}
	If $ g_{j}^{min}(i_{1},...,i_{j}) $ is monotonically
	non-descreasing for $ i_{j} $ if 
	$ f_{j}^{max}(i_{1},...,i_{j}) <= g_{j}^{min}(i_{1},...,i_{j}+1)$
	1) forall $ i_{1},...,i_{j} \in \Re_{j} $ and for
	$ 0 <= i_{j} <= N_{j} - 1 $, then there can be no
	dependencies from $ A(f(\overrightarrow{i})) $ to
	$ A(g(\overrightarrow{i})) $ with a direction vector
	$ \overrightarrow{d} $ of the form
	$ d_{1} = '=',..., d_{j-1} = '=', d_{j} = '<' $
\end{theorem}


\begin{theorem}
	If $ g_{j}^{min}(i_{1},...,i_{j}) $ is monotonically
	non-increasing for $ i_{j} $ if 
	$ f_{j}^{max}(i_{1},...,i_{j}) <= g_{j}^{min}(i_{1},...,i_{j}-1)$
	1) forall $ i_{1},...,i_{j} \in \Re_{j} $ and for
	$ 0 <= i_{j} <= N_{j} - 1 $, then there can be no
	dependencies from $ A(f(\overrightarrow{i})) $ to
	$ A(g(\overrightarrow{i})) $ with a direction vector
	$ \overrightarrow{d} $ of the form
	$ d_{1} = '=',..., d_{j-1} = '=', d_{j} = '<' $
\end{theorem}

Those theorems can be used to prove independence when the
values of access functions $ f $ and $ g $ are interleave,
but the values taken by $ f $ and $ g $ for a loop iteration
are not interleaved with values taken by other iterations.
This case is presented in the following diagram:

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.5]{./Diagrams/RangeTestExample2.eps}
	\caption{$ f(i,j) = 2*N*i+j $, $ g(i, j) = 2*N*i + j + N $}
\end{figure}

Unfortunately, presented three tests cannot disprove dependencies
for complex interleavings, but in 
\cite{TheRangeTestADependenceTestForSymbolNonLinearExpr} it is observed
that usually interleavings can be eliminated by permuting nested loops.

\subsection{Barvinok Counting}
Barvinok Counting is an algorithm for counting the integral
points of a convex rational polytope. It is used in parallel
compilers for counting number of solutions for data dependency
problems. According to \cite{IntegerProgrammingAndLatticePointEnumeration} 
Barvinok Counting can be divided
into two steps. In the first step we create generating function
for given polytope. To be more precise the definition for generating
function is:


\begin{defn}
	Let $ P $ be a (rational) polyhedron. We define the
	multivariate generating function as:

	\begin{align}
		\begin{split}
			f(P) := \sum_{a \in P \cup Z^{d}}{}z^{a}
		\end{split}
	\end{align}

	where we use the notation that $ z^{a} = z_{1}^{\alpha_{1}}...z_{d}^{\alpha_{d}}$
\end{defn}

Having generating function $ f $ we calculate the number of lattice
points by evaluating $ f(P) $ at $ z = 1 = (1,1,...,1)^{T}$.
The biggest challenge is finding
this multivariate generating function. For example for
triangle in the following example:

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.5]{./Diagrams/BarvinokExample.eps}
	\caption{Barvinok Example}
\end{figure}

The generating function is 
$ f(x_{1}, x_{2}) = x_{1}^{0}x_{2}^{0}+x_{1}^{1}x_{2}^{0}+x_{1}^{2}x_{2}^{0} +
                    x_{1}^{0}x_{2}^{1}+x_{1}^{1}x_{2}^{1}+x_{1}^{0}x_{2}^{2} $

More information on finding multivariate generating function
can be found in 
\cite{CountingIntegerPointsInParametricPolytopesUsingBarvinokRationalFunctions}.

\section{Data dependency vectors}
When there are dependencies between instructions in loop,
we are additionally interested in properties of this dependency.
This information can be used in calculating which iterations
carry dependency and in the result enable us to make loop
run in parallel for the sets of iteration that does not depend on
each other. The are two important types of information about
this:

\begin{enumerate}
	\item Distance vectors - contain information about the
	      shape of the dependency
	\item Direction vectors - contain information about the
	      direction of the dependency
\end{enumerate}

 The formal definition of the distance vector can be described \cite{AdvancingDataDependenceAnalysisInPractice} as follows:

\begin{defn}
	Suppose two statements share common loops $ I_{1} $,
	$ I_{2} $,...,$ I_{n} $ and there are dependency between
	them in iterations:
	\begin{align}
		\begin{split}
			i = (i_{1}, i_{2},..., i_{n}) \\
			i' = (i'_{1}, i'_{2},..., i'_{n}) 
		\end{split}
	\end{align}
	and $ i' > i $ in lexicographical order. We define
	the distance vector of dependency as:
	\begin{align}
		\begin{split}
			d = i' - i = (i'_{1} - i_{1}, i'_{2} - i_{2},..., i'_{n} - i_{n})
		\end{split}
	\end{align}
\end{defn}

Many loop transformation algorithms does not require information
about exact distances between iterations but the direction of
the dependency. This subset of distance vector is called the
direction vector and can be expressed formally as:

\begin{defn}
	Suppose two statements share common loops $ I_{1} $,
	$ I_{2} $,...,$ I_{n} $ and there are dependency between
	them in iterations:
	\begin{align}
		\begin{split}
			i = (i_{1}, i_{2},..., i_{n}) \\
			i' = (i'_{1}, i'_{2},..., i'_{n}) 
		\end{split}
	\end{align}
	We define the distance vector of dependency as:
	\begin{align}
		\begin{split}
			v = (v_{1}, v_{2}, ..., v_{n}) \text{ where } v_{k} \in \{<, =, >\}, 1<=k<=n \text{ and} \\
			v_{k} = 
				\begin{cases}
					< \quad \text{if } i_{k} < i'_{k} \\
					= \quad \text{if } i_{k} = i'_{k} \\
					> \quad \text{if } i_{k} > i'_{k} \\
				\end{cases}
		\end{split}
	\end{align}
\end{defn}

\section{Loop Transformations}
Loop transformation is the process of changing the structure of the loop
while preserving loop invariants to keep old behavior. Loop transformation
is usually done to make loop run more efficiently. Transforming the loop
can be done to expose parallelism that is not available in the original
form. Transformation is also done to improve locality of reference 
by arranging data to take advantage of CPU caching. In the following
subsections there are described several popular loop transformations,
more can be found in \cite{LoopTransformationsLecture}.

\subsection{Loop Distribution}
Known also as loop fission, it works by dividing loop control over
different statement in the loop body. It changes the following
loop:

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}[]
for (i=1;i<=30;i++) {
   e[i+3] = b[i-1];
   a[i] = a[i-1] + 5;
}
\end{lstlisting}
\end{minipage}

into the following loops:

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}[]
for (i=1;i<=30;i++) 
   e[i+3] = b[i-1];
for (i=1;i<=30;i++)
   a[i] = a[i-1] + 5;

\end{lstlisting}
\end{minipage}

This transformation is used for several reasons\cite{LoopTransformationsLecture}:

\begin{enumerate}
	\item Isolating data dependencies cycles
	\item Enabling other transformations
	\item Improving data locality
	\item Separate different data streams in a loop to improve hardware prefetch
\end{enumerate}

\subsection{Loop Fusion}
This transformation joins adjacent loops that have the same loop
bounds into one loop.


\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}[]
for (i=1;i<=30;i++) {
   a[i] = 15*i + i;
}
for (i=1;i<=30;i++) {
   b[i] = i + 3;
}

\end{lstlisting}
\end{minipage}

into the following one:

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}[]
for (i=1;i<=30;i++) {
   a[i] = 15*i + i;
   b[i] = i + 3;
}
\end{lstlisting}
\end{minipage}

This transformation is used for several reasons\cite{LoopTransformationsLecture}:

\begin{enumerate}
	\item Reducing loop overhead
	\item Increasing the granularity of work done in a loop
	\item Improving locality of combining loops that reference the same array
\end{enumerate}

\subsection{Loop Peeling}
Loop Peeling is loop transformation that move first or last iteration
of the loop outside of the loop. For example it changes:

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}[]
for (i=0;i<=10;i++) {
   a[i] = b[i+2];
}
\end{lstlisting}
\end{minipage}

into the following loops:


\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}[]
a[0] = b[0+2];
for (i=1;i<=10;i++) {
   a[i] = b[i+2];
}
\end{lstlisting}
\end{minipage}

This transformation is used to
enforce a particular initial memory alignment on array
references prior to loop vectorization\cite{LoopTransformationsLecture}

\subsection{Loop Unrolling}
Also known as loop unwinding is a loop transformation that optimizes
the program execution speed increasing its binary size. It works
by combining loop iteration together with changing strip count
of loop iteration variable. For example it changes:


\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}[]
a = 1;
for (i=1;i<8;i++) {
   a *= b[i];
}
\end{lstlisting}
\end{minipage}

into the following:


\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}[]
sum = 1;
for (i=0;i<8;i+=4) {
   a *= b[i];
   a *= b[i+1];
   a *= b[i+2];
   a *= b[i+3];
}
\end{lstlisting}
\end{minipage}

This transformation is exposing more possibility for
instruction level parallelism, decreases loop control overhead (eliminates number of jump instruction in assembly code).

\subsection{Loop Interchanging}
Loop interchange is exchanging order of iteration in nested loops.
It changes the following code:

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}[]
for (i=0;i<=20;i++)
   for (j=0;j<=30;j++)
      sum[i] += j;
\end{lstlisting}
\end{minipage}

into the following:

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}[]
for (j=0;j<=30;j++)
   for (i=0;i<=20;i++)
      sum[i] += j;
\end{lstlisting}
\end{minipage}

This transformation can create opportunities to further optimize,
using vectorization of the array assignments, improves memory access locality.

\subsection{Loop Tiling}
Loop tiling is another example of loop transformation that increases the locality
of reference. In the result data is more frequently get from cache which increases
performance of the loop. This transformation divides iteration space into smaller
blocks that fits the cache size and decreases cache miss ratio and also is useful
for efficient vectorization on SSE, AVX modules. Usually the block
size is choose to fit processor cache size or vector word size. An ordinary loop:

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}[]
for (i=0;i<2500;i++) {
   A[i] = A[i] + 15;	
}
\end{lstlisting}
\end{minipage}

 can be blocked with a block size B by replacing it with:


\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}[]
for(j=0;j<2500;j+=block_size) {
   for (i=j;i<min(2500, j+block_size);i++) {
      A[i] = A[i] + 15;
   }
}
\end{lstlisting}
\end{minipage}

This transformation is used for several reasons\cite{LoopTransformationsLecture}:

\begin{enumerate}
	\item make execution of certain types of loops more efficient by increasing
	      the locality of reference
          \item make vectorization more efficient (block size should be divisible by vector word size)
\end{enumerate}

\subsection{Loop-invariant code motion}
Loop-invariant code motion is an optimization
that moves loop-invariant code outside of loop.
Loop-invariant is a code that is true before
and after each iteration of the loop. For example
for the following loop:

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}[]
for (int i=0;i < n;i++) {
   x = y + z;
   a[i] = 6 * i + x * x;
}
\end{lstlisting}
\end{minipage}

We can move two instructions \lstinline|x = y + z| and
\lstinline|x * x| can be moved outside of the loop since
winthin they are loop-invariant code, so the optimized code
will look like the following:

\begin{minipage}{\linewidth}
\lstset{language=C}
\begin{lstlisting}[]
x = y + z;
t1 = x * x;
for (int i=0;i < n;i++) {
   a[i] = 6 * i + t1;
}
\end{lstlisting}
\end{minipage}

Loop-invariant code which has been hoisted out of a loop is 
executed less often providing a speedup. Another effect of
transformation is allowing constants to be stored in registers
and not having to calculate the address and access the memory
at each iteration. \\ 
However, if too many variables are created, there will be high register
pressure especially on processors with few registers, like the 32bit x86.
If the compiler runs out of register, some variables will be spilled.
To counteract this, the inverse optimization can be performed - rematerialization.














